<?php
/**
 * The template for displaying /pform-mushi
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package zero_to_one
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<!--pankuzuここから-->
				<div class="pankuzu">
					<ol class="pankuzu-list">
						<li><a href="<?php echo home_url();?>">ホーム</a></li>
						<li><?php echo get_the_title(); ?></li>
					</ol>
				</div>
			<!--pankuzuここまで-->

			<!-- <h1 class="categorypage-ttl"><strong>虫とら　お見積りフォーム</strong>
			<span>「虫とら」製品に関するお問い合わせ・お見積り</span></h1> -->

			<section class="inner-wrap--min">
				<div class="detail-area">
					<div class="detail-content">

						<!--pardot-->

							<noscript>
							<iframe src="https://go.pardot.com/l/209362/2017-09-08/69g54" width="100%" height="800" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe>
							</noscript>

							<script type="text/javascript">
							var form = 'https://go.pardot.com/l/209362/2017-09-08/69g54';
							var params = window.location.search;
							var thisScript = document.scripts[document.scripts.length - 1];
							var iframe = document.createElement('iframe');

							iframe.setAttribute('src', form + params);
							iframe.setAttribute('width', '100%');
							iframe.setAttribute('height', 1720);
							iframe.setAttribute('type', 'text/html');
							iframe.setAttribute('frameborder', 0);
							iframe.setAttribute('allowTransparency', 'true');
							iframe.style.border = '0';

						  thisScript.parentElement.replaceChild(iframe, thisScript);
						  </script>

						<!-- pardot -->

					</div>
				</div>


			<?php
			get_sidebar(); ?>

			</section><!-- ./inner-wrap-min-->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
