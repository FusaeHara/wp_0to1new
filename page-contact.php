<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package zero_to_one
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<!--pankuzuここから-->
				<div class="pankuzu">
					<ol class="pankuzu-list">
						<li><a href="<?php echo home_url();?>">ホーム</a></li>
						<li><?php echo get_the_title(); ?></li>
					</ol>
				</div>
			<!--pankuzuここまで-->

			<h1 class="categorypage-ttl"><strong>Contact</strong>
			<span>各種お問い合わせ</span></h1>

			<section class="inner-wrap--min">
				<div class="detail-area">
					<div class="detail-content">

						<!--pardot-->
						<div class="pardot-wrapper">

							<!--PC-->

							<div class="pc-on">
							<form action="https://pro.form-mailer.jp/fm/service/Forms/confirm" method="post" name="form1" ENCTYPE="multipart/form-data" accept-charset="UTF-8">
							<input type="hidden" name="key" value="d3082285230274">
							<p class="mb20" style="background:#eee; display:block; text-align:center;">*は必須項目です</p>
							<!-- text -->
							<p class="form-title">
								*御社名
							</p>
							<p>
								<input name="field_3592793" type="text" value="" size="30" >
							</p>
							<!-- text -->
							<p class="form-title">
								*業種
							</p>
							<p>
								<input name="field_3592794" type="text" value="" size="30" >
							</p>
							<!-- text -->
							<p class="form-title">
								*ご担当者名
							</p>
							<p>
								<input name="field_3592795" type="text" value="" size="30" >
							</p>
							<!-- text -->
							<p class="form-title">
								*ご担当者さまの職務
							</p>
							<p>
								<input name="field_3592796" type="text" value="" size="30" >
							</p>
							<!-- email -->
							<p class="form-title">
								*メールアドレス
							</p>
							<p>
								<input name="field_3592797" type="text" size="30">
							</p>
							<!-- phone -->
							<p class="form-title">
								*電話番号
							</p>
							<p>
								<input name="field_3592798_1" type="text" maxlength="4">-<input name="field_3592798_2" type="text" maxlength="4">-<input name="field_3592798_3" type="text" maxlength="4">
							</p>
							
							<!-- textarea -->
							<p class="form-title">
								ご要望をご記入ください
							</p>
							<p>
								<textarea name="field_3592809" cols="30" rows="5"></textarea>
							</p>
							<p class="submit-btn">
								<input name="submit" type="submit" value="確認画面へ">
							</p>
							<input type="hidden" name="code" value="utf8">
							</form>
							</div>


							<!-- SP -->
							
							<div class="sp-on">

							<form action="https://pro.form-mailer.jp/fm/service/Forms/confirm" method="post" name="form1" accept-charset="UTF-8">
								<input type="hidden" name="key" value="d3082285230274">
								<input type="hidden" name="codehint" value="utf8">
								<p class="mb20" style="background:#eee; display:block; text-align:center;">*は必須項目です</p>
								<!-- text -->
								<p class="form-title">
									*御社名
								</p>
								<p>
									<input name="field_3592793" type="text" value="" size="30"  >
								</p>
								<br>
								<!-- text -->
								<p class="form-title">
									*業種
								</p>
								<p>
									<input name="field_3592794" type="text" value="" size="30"  >
								</p>
								<br>
								<!-- text -->
								<p class="form-title">
									*ご担当者名
								</p>
								<p>
									<input name="field_3592795" type="text" value="" size="30"  >
								</p>
								<br>
								<!-- text -->
								<p class="form-title">
									*ご担当者さまの職務
								</p>
								<p>
									<input name="field_3592796" type="text" value="" size="30"  >
								</p>
								<br>
								<!-- email -->
								<p class="form-title">
									*メールアドレス
								</p>
								<p>
									<input name="field_3592797" type="text" size="30" istyle="3">
								</p>
								<br>
								<!-- phone -->
								<p class="form-title">
									*電話番号
								</p>
								<p>
									<input name="field_3592798_1" type="text" maxlength="4" istyle="4"> - <input name="field_3592798_2" type="text" maxlength="4" istyle="4"> - <input name="field_3592798_3" type="text" maxlength="4" istyle="4">
								</p>
								<br>
								<!-- textarea -->
								<p class="form-title">
									ご要望をご記入ください
								</p>
								<p>
									<textarea name="field_3592809" cols="30" rows="5" ></textarea>
								</p>
								<p class="submit-btn">
									<input name="submit" type="submit" value="確認画面へ">
								</p>
							</form>

							</div>

						</div>

						<!-- pardot -->

					</div>
				</div>


			</section><!-- ./inner-wrap-min-->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
