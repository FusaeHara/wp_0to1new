<?php
/**
 * The template for displaying solution pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package zero_to_one
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<!--pankuzuここから-->
			<?php $postcate = get_the_category(); ?>
				<div class="pankuzu">
					<ol class="pankuzu-list">
						<li><a href="<?php echo home_url();?>">ホーム</a></li>
						<li><?php echo single_cat_title();?></li>
					</ol>
				</div>
			<!--pankuzuここまで-->

			<h1 class="categorypage-ttl categorypage-ttl--border">
				<strong>Works</strong>
				<span>納入事例</span>
			</h1>

			<div class="inner-wrap">

			<!--防虫-->
				<h3 class="card-list-ttl"><span>衛生分野</span></h3>
				<div class="card-list">

					<?php
					$cat = get_the_category();
					$cat = $cat[0];

					//親カテゴリがあるか
					if($cat->category_parent){
						$parent_cat = get_category($cat->category_parent);
						$catname = $parent_cat->name;
						$catslug = $parent_cat->slug;
					}else{
						$catname = $cat->name;//カテゴリー名
						$catslug = $cat->slug;//スラッグ名
					}
					?>

					<?php
					$args = array( 'posts_per_page' => 8, 'category_name' => 's-pest', 'orderby'=> 'date','order'=> 'DESC' );
					$wnposts = get_posts( $args );
					foreach ( $wnposts as $post ) : setup_postdata( $post ); ?>

							<article class="card-article label-solution">
								<a href="<?php the_permalink(); ?>">
									<div class="card-article-thum"><?php the_post_thumbnail(); ?></div>
									<div class="<?php echo $catslug;?> card-article-label"><?php echo $catname;?></div>
									<div class="card-article-data"><?php the_date();?></div>
									<div class="card-article-text"><h2><?php the_title(); ?></h2></div>
								</a>
								<?php the_tags( '<ul class="card-article-tag"><li>', '</li><li>', '</li></ul>' );?>
								</article>
  				<?php endforeach; wp_reset_postdata();?>
					</div><!--./cardlist --><!--./防虫-->



					<!--検査分野-->
									<h3 class="card-list-ttl"><span>検査分野</span></h3>
									<div class="card-list">

										<?php
										$args = array( 'posts_per_page' => 8, 'category_name' => 's-highcolor', 'orderby'=> 'date','order'=> 'DESC' );
										$wnposts = get_posts( $args );
										foreach ( $wnposts as $post ) : setup_postdata( $post ); ?>


												<article class="card-article label-solution">
													<a href="<?php the_permalink(); ?>">
														<div class="card-article-thum"><?php the_post_thumbnail(); ?></div>
														<div class="<?php echo $catslug;?> card-article-label"><?php echo $catname;?></div>
														<div class="card-article-data"><?php the_date();?></div>
														<div class="card-article-text"><h2><?php the_title(); ?></h2></div>
													</a>
													<?php the_tags( '<ul class="card-article-tag"><li>', '</li><li>', '</li></ul>' );?>
													</article>
										<?php endforeach; wp_reset_postdata();?>

									</div><!--./cardlist --><!--./高演色性光源-->



			</div><!--./inner-wrap-->


		</main><!-- #main -->
	</div><!-- #primary -->


<?php
get_footer();
