<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package zero_to_one
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<!--pankuzuここから-->
			<?php
			$category_id = get_queried_object();
			 ?>

				<div class="pankuzu">
					<ol class="pankuzu-list">
						<li><a href="<?php echo home_url();?>">ホーム</a></li>
						<li><?php echo get_category_parents($category_id,TRUE,' ＞ '); ?></li>
					</ol>
				</div>
			<!--pankuzuここまで-->

		<?php
		if ( have_posts() ) : ?>

			<h1 class="categorypage-ttl categorypage-ttl--border">
				<?php
					//アーカイブタイトル（：）なし
					$pattern = '/.+?: /';
					$title = preg_replace($pattern,'',get_the_archive_title(),1);
					echo 	'<strong>'.$title.'</strong>';
					//アーカイブDescription
					the_archive_description( '<span>', '</span>' );
				?>
			</h1><!-- .page-header -->

			<div class="inner-wrap">
				<div class="card-list">
						<?php while ( have_posts() ) : the_post();?>

							<?php
			        $cat = get_the_category();
			        $cat = $cat[0];

			        //親カテゴリがあるか
			        if($cat->category_parent){
			          $parent_cat = get_category($cat->category_parent);
			          $catname = $parent_cat->name;
			          $catslug = $parent_cat->slug;
			        }else{
			          $catname = $cat->name;//カテゴリー名
			          $catslug = $cat->slug;//スラッグ名
			        }
			        ?>

							<article class="card-article">
								<a href="<?php the_permalink(); ?>">
									<div class="card-article-thum"><?php the_post_thumbnail(); ?></div>
									<div class="<?php echo $catslug;?> card-article-label"><?php echo $catname;?></div>
									<div class="card-article-data"><?php the_time('Y-m-d');?></div>
									<div class="card-article-text"><h2><?php the_title(); ?></h2></div>
								</a>
								<?php the_tags( '<ul class="card-article-tag"><li>', '</li><li>', '</li></ul>' );?>
								</article>

							<?php endwhile;
							the_posts_navigation();
						else :
							get_template_part( 'template-parts/content', 'none' );
						endif; ?>

				</div><!--./cardlist -->
			</div><!--./inner-wrap-->


		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
