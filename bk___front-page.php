<?php
/**
 * The template for displaying front pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package zero_to_one
 */

get_header(); ?>

  <main class="top-area dis-flex">

    <div class="main-banner">
    <div class="slider-logo"><img src="<?php echo get_stylesheet_directory_uri();?>/img/top/title_works.png" alt="WORKS" /></div>
    <?php get_template_part( 'template-parts/main','banner' ); ?>
    </div>

    <ul class="sub-banner">
      <?php get_template_part( 'template-parts/sub','banner' ); ?>
    </ul>

  </main>

  <!--What's new-->
  <section class="sec-whatsnew sec">
    <h2 class="sec-ttl"><strong>What’s NEW</strong></h2>

    		<ul class="whatsnew-list">
          <?php
          $args = array( 'posts_per_page' => 3, 'category_name' => 'whatsnew', 'orderby'=> 'date','order'=> 'DESC' );
          $wnposts = get_posts( $args );
          foreach ( $wnposts as $post ) : setup_postdata( $post ); ?>
          <li>
          <?php  $icon = get_field('icon-wn'); ?>
              <dl><dt><?php the_time('Y-m-d');?></dt><dd><img src="<?php echo get_stylesheet_directory_uri();?>/img/top/icon-<?php echo $icon; ?>.png" /></dd><dd><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></dd></dl>
          </li>
          <?php endforeach; wp_reset_postdata();?>
        </ul>

        <a href="<?php echo esc_url( home_url( '/' ) ); ?>category/whatsnew/" class="btn btn-more whatsnew-more">一覧</a>
    </section>
    <!-- ./What'snew ->

    <!---Products -->


	  <section class="sec-products sec">
    		<h2 class="sec-ttl"><strong>Products</strong><span>新しい驚きを、さまざまな市場で</span></h2>
    		<a href="<?php echo esc_url( home_url( '/' ) ); ?>category/products/" class="btn btn-more products-more">プロダクト一覧</a>
    	</section>
    <!-- ./Products -->

    <!--Solution -->
    	<section class="sec-solution sec">
    		<h2 class="sec-ttl"><strong>Works</strong><span>納入事例</span></h2>
    		<div class="solution-inner">
    			<ul class="solution-tab">
    				<li class="active">
    					<a href="###">
    						<div class="pc-on"><img src="<?php echo get_stylesheet_directory_uri();?>/img/common/solution_icon01.svg"></div>
    						<div class="sp-on"><a href="<?php echo esc_url( home_url( '/' ) ); ?>category/solution/s-pest/" class="solution-icon solution-icon1"><img src="<?php echo get_stylesheet_directory_uri();?>/img/solution/thum_pesticide.png"></a></div>
    						<p class="solution-tab-ttl">衛生分野</p>
    					</a>
    				</li>
    				<!-- <li>
    					<a href="###">
    						<div class="pc-on"><img src="<?php echo get_stylesheet_directory_uri();?>/img/common/solution_icon02.svg"></div></a>
    						<div class="sp-on"><a href="<?php echo esc_url( home_url( '/' ) ); ?>category/solution/s-sleep/" class="solution-icon solution-icon2"><img src="<?php echo get_stylesheet_directory_uri();?>/img/solution/thum_sleep.png"></a></div>
    						<p class="solution-tab-ttl">睡眠</p>
    					</a>
    				</li> -->
    				<li>
    					<a href="###">
    						<div class="pc-on"><img src="<?php echo get_stylesheet_directory_uri();?>/img/common/solution_icon03.svg"></div>
    						<div class="sp-on"><a href="<?php echo esc_url( home_url( '/' ) ); ?>category/solution/s-highcolor/" class="solution-icon solution-icon3"><img src="<?php echo get_stylesheet_directory_uri();?>/img/solution/thum_source.png"></a></div>
    						<p class="solution-tab-ttl">検査分野</p>
    					</a>
    				</li>
    				<!-- <li>
    					<a href="###">
    						<div class="pc-on"><img src="<?php echo get_stylesheet_directory_uri();?>/img/common/solution_icon04.svg"></div>
    						<div class="sp-on"><a href="<?php echo esc_url( home_url( '/' ) ); ?>category/solution/s-vr/" class="solution-icon solution-icon4"><img src="<?php echo get_stylesheet_directory_uri();?>/img/solution/thum_vr.png"></a></div>
    						<p class="solution-tab-ttl">3D/VR</p>
    					</a>
    				</li> -->
    			</ul>
    		</div>
    		<div class="spec-area">
    			<article class="spec">
    				<h3 class="spec-ttl">衛生分野</h3>
    				<p class="spec-lead">主に食品工場の内部・製造ラインや、レストランなど食べ物を扱う店舗様に向けて、薬品ではなく”光”を使った捕虫器など、画期的な防虫・捕虫対策をご提案しています。</p>
    				<div class="spec-btn">
    					<a href="<?php echo esc_url( home_url( '/' ) ); ?>category/solution/s-pest/" class="btn btn-more spec-more">「衛生分野」の記事</a>
    					<a href="<?php echo esc_url( home_url( '/' ) ); ?>category/products/p-pest/" class="btn btn-more spec-more">「衛生分野」のプロダクト</a>
    				</div>
    			</article>
    			<!-- <article class="spec">
    				<h3 class="spec-ttl">睡眠</h3>
    				<p class="spec-lead">青色光が睡眠に必要な脳内物質「メラトニン」の分泌に影響していることに着目し、大学や研究機関の監修のもと、快適な眠りをサポートする間接照明やスタンドライトの開発を行っています。</p>
    				<div class="spec-btn">
    					<a href="<?php echo esc_url( home_url( '/' ) ); ?>category/solution/s-sleep/" class="btn btn-more spec-more">「睡眠」の記事</a>
    					<a href="<?php echo esc_url( home_url( '/' ) ); ?>category/products/p-sleep/" class="btn btn-more spec-more">「睡眠」のプロダクト</a>
    				</div>
    			</article> -->
    			<article class="spec">
    				<h3 class="spec-ttl">検査分野</h3>
    				<p class="spec-lead">製品メーカーや医療施設、デザインの現場など、厳密な色校正を必要とする施設において、屋内でも自然光（太陽光）に近い色の発色を実現する、高演色な光源の開発を行っています。</p>
    				<div class="spec-btn">
    					<a href="<?php echo esc_url( home_url( '/' ) ); ?>category/solution/s-highcolor/" class="btn btn-more spec-more">「検査分野」の記事</a>
    					<a href="<?php echo esc_url( home_url( '/' ) ); ?>category/products/p-highcolor/" class="btn btn-more spec-more">「検査分野」のプロダクト</a>
    				</div>
    			</article>
    			<article class="spec">
    				<h3 class="spec-ttl">3D/VR</h3>
    				<p class="spec-lead">空間の3Dデータをローコスト＆短納期でモデリング。歪みの少ない3Dデータは、結婚式場や不動産の集客や社内の営業ツールと利用するほか、ホール施設の顧客誘致、文化遺産の空間データ保存などお客様の空間をコンサルティングする際に有効な手段としてもお使いいただけます。</p>
    				<div class="spec-btn">
    					<a href="<?php echo esc_url( home_url( '/' ) ); ?>category/solution/s-vr/" class="btn btn-more spec-more">「3D/VR」の記事</a>
    					<a href="<?php echo esc_url( home_url( '/' ) ); ?>category/products/p-vr/" class="btn btn-more spec-more">「3D/VR」のプロダクト</a>
    				</div>
    			</article>
    		</div>
    	</section>
    <!-- ./Solution -->

    <!-- Contents -->
    <section class="contents sec">
      <h2 class="sec-ttl"><strong>Contents</strong><span>最近の記事一覧</span></h2>
      <div class="card-list">

        <!--loop start -->

        <?php
        $args = array( 'posts_per_page' => 8, 'category_name' => 'products,solution,whatsnew', 'orderby'=> 'date','order'=> 'DESC' );
        $wnposts = get_posts( $args );
        foreach ( $wnposts as $post ) : setup_postdata( $post ); ?>

        <?php
        $cat = get_the_category();
        $cat = $cat[0];

        //親カテゴリがあるか
        if($cat->category_parent){
          $parent_cat = get_category($cat->category_parent);
          $catname = $parent_cat->name;
          $catslug = $parent_cat->slug;
        }else{
          $catname = $cat->name;//カテゴリー名
          $catslug = $cat->slug;//スラッグ名
        }
        ?>

        <article class="card-article">
          <a href="<?php the_permalink(); ?>">
            <div class="card-article-thum"><?php the_post_thumbnail(); ?></div>
            <div class="<?php echo $catslug;?> card-article-label"><?php echo $catname;?></div>
            <div class="card-article-data"><?php the_time('Y-m-d');?></div>
            <div class="card-article-text"><h3><?php the_title(); ?></h3></div>
          </a>
          <?php the_tags( '<ul class="card-article-tag"><li>', '</li><li>', '</li></ul>' );?>
          </article>
          <?php endforeach; wp_reset_postdata();?>

        <!--loop end -->

      </div><!-- ./card-list -->

      <a href="<?php echo esc_url( home_url( '/' ) ); ?>/allarchive/" class="btn btn-more contents-more">すべての記事をみる</a>
      <!-- ./Contents -->


<?php
  get_footer();
