<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package zero_to_one
 *
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<!--pankuzuここから-->
			<?php
				$cat = get_the_category();
				$cat = $cat[0];

				//親カテゴリがあるか
				if($cat->category_parent){
					$parent_cat = get_category($cat->category_parent);
					$catname = $parent_cat->name;
					$catslug = $parent_cat->slug;
				}else{
					$catname = $cat->name;//カテゴリー名
					$catslug = $cat->slug;//スラッグ名
				}

			?>
				<div class="pankuzu">
					<ol class="pankuzu-list">
						<li><a href="<?php echo home_url();?>">ホーム</a></li>
						<li><a href="<?php echo home_url();?>/category/<?php echo $catslug;?>/"><?php echo $catname;?></a></li>
						<li><?php echo get_the_title();?></li>
					</ol>
				</div>
			<!--pankuzuここまで-->

		<section class="inner-wrap--min">

		<?php
		while ( have_posts() ) : the_post();


			get_template_part( 'template-parts/content', get_post_format() );


		endwhile; // End of the loop.
		?>


		<?php
		get_sidebar(); ?>

		</section><!-- ./inner-wrap-min-->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
