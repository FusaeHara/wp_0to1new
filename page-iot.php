<?php
/**
 * The template for displaying All archives pages
 * Template Name:page-iot
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package zero_to_one
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<!--pankuzuここから-->
			<?php
			$category_id = get_queried_object();
			 ?>

				<div class="pankuzu">
					<ol class="pankuzu-list">
						<li><a href="<?php echo home_url();?>">ホーム</a></li>
						<li>IoT</li>
					</ol>
				</div>
			<!--pankuzuここまで-->

			<h1 class="header--grad flexbox" id="gradient">
				<span class="header--grad__ttl">IoT</span>
			</h1><!-- .page-header -->

			<div class="inner-wrap lay-diot flexbox">

				<!--　▼ content area ▼ -->

				<div class="detail-area">
						<?php
						$args = array(
							  'post_type' => 'iot', /* カスタム投稿名 */
							  'posts_per_page' => 10, /* 表示する数 */
						); ?>

						<?php $my_query = new WP_Query( $args ); ?>

						<ul>

						<?php while ( $my_query->have_posts() ) : $my_query->the_post(); ?>

						<li><h2 class="lay-diot__sub-column__h"><?php the_title();?></h2>

						<div class="lay-diot__sub-column" id="anc-<?php the_ID() ;?>">
								<style>
								img {width: auto;}
								</style>


								<!-- 電源コラム　-->　

								<?php if(have_rows('diot-sub-column')): ?>
								<?php while(have_rows('diot-sub-column')): the_row(); ?>



									<?php
									$title = get_sub_field('diot-sub-title');
									$wyz = get_sub_field('diot-sub-wyz');
									?>

									<?php remove_filter('the_content', 'wpautop');  //editorのpタグを削除 ?>

									<div class="lay-diot__sub-column__block clearfix" id="<?php the_sub_field('diot-sub-title'); ?>"  style="display:block;">
									<h3 class="lay-diot__sub-column__subtitle"><?php echo $title; ?></h3>
									<?php if($wyz){ ?><div><?php echo $wyz; ?></div><? } ?>
									</div>


								<?php endwhile; ?>
								<?php endif; ?>

						</div><!--lay-diot__sub-column -->

						</li>

						<?php endwhile; ?>

						</ul>

						<?php wp_reset_postdata(); ?>


				</div>

				<!--　▲ content area ▲ -->

				<!--　▼ side anchor area ▼ -->

					<div class="lay-diot__sidenav">

					<h2 class="lay-diot__sidenav__title">目次</h2>

						<?php
						$args = array(
								'post_type' => 'iot', /* カスタム投稿名 */
								'posts_per_page' => 10, /* 表示する数 */
						); ?>

						<?php $my_query = new WP_Query( $args ); ?>

						<ul class="lay-diot__sidenav__link">


						<?php while ( $my_query->have_posts() ) : $my_query->the_post(); ?>

							<li class="lay-diot__sidenav__link__item">


							<a class="lay-diot__sidenav__link__item__maina" href="#anc-<?php the_ID(); ?>"><?php the_title();?></a>

							<?php if(have_rows('diot-sub-column')): ?>
							<?php while(have_rows('diot-sub-column')): the_row(); ?>

								<a class="lay-diot__sidenav__link__item__a" href="#<?php the_sub_field('diot-sub-title'); ?>"><?php the_sub_field('diot-sub-title'); ?></a>

							<?php endwhile; ?>
							<?php endif; ?>


							</li>

						<?php endwhile; ?>

					</ul>
					<?php wp_reset_postdata(); ?>

					</div>


				<!--　▲ side anchor area ▲ -->

			</div><!--./inner-wrap-->


		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
