<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package zero_to_one
 *
 * WP Post Template: 商品紹介テンプレート
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<!--pankuzuここから-->
			<?php
				$cat = get_the_category();
				$cat = $cat[0];

				//親カテゴリがあるか
				if($cat->category_parent){
					$parent_cat = get_category($cat->category_parent);
					$catname = $parent_cat->name;
					$catslug = $parent_cat->slug;
				}else{
					$catname = $cat->name;//カテゴリー名
					$catslug = $cat->slug;//スラッグ名
				}

			?>
				<div class="pankuzu">
					<ol class="pankuzu-list">
						<li><a href="<?php echo home_url();?>">ホーム</a></li>
						<li><a href="<?php echo home_url();?>/category/<?php echo $catslug;?>/"><?php echo $catname;?></a></li>
						<li><?php echo get_the_title();?></li>
					</ol>
				</div>
			<!--pankuzuここまで-->

		<?php
		while ( have_posts() ) : the_post();?>


			<article id="post-<?php the_ID(); ?>" <?php post_class('detail-area-2'); ?>>

				<div class="proLP-inner-wrap">

					<div class="proLP-inner-head">

					<?php
					$detaillogo = get_field('detaillogo');
					$logourl = wp_get_attachment_image_src($detaillogo, 'full');
					if($logourl){ ?>

						<h1 class="proLP-detail-logo"><img src="<?php echo
						$logourl[0]; ?>" alt="<?php the_title_attribute(); ?>"></h1>

					<?php } ?>

					<?php
						$kanrenimg = get_field('kanrenimg');
							if($kanrenimg){ ?>

								<a class="proLP-detail-kanren" href="<?php echo get_field('kanrenurl') ?>">
									<div class="proLP-detail-kanren__inner">
										<h2 class="proLP-detail-kanren__title">関連商品</h2>
										<p><?php echo get_field('kanrentxt'); ?>>></p>
									</div>
									<div class="proLP-detail-kanren__inner">
										<p style="border: 1px solid #CCC;"><img src="<?php echo get_field('kanrenimg');?>"></p>
									</div>
								</a>

							<?php } ?>

				</div>

					<!-- -->


					  <div class="proLP-detail-pimg">

							<?php
								$mainimg = get_field('p-mainimg');
									if($mainimg){ ?>
									<img src="<?php echo $mainimg; ?>" alt="<?php the_title_attribute(); ?>">
							<?php } ?>

			  		</div>


						<div class="proLP-detail-textarea">

						<?php
							$detailtitle = get_field('detailtitle');
								if($detailtitle){ ?>
									<h2 class="proLP-detail-textarea-subttl">
										<?php echo $detailtitle; ?>
									</h2>
							<?php } ?>

							<?php
								$detaildesc = get_field('detaildesc');
									if($detaildesc){ ?>
										<p class="proLP-detail-textarea-desc">
											<?php echo $detaildesc; ?>
										</p>
								<?php } ?>


							<ul class="proLP-detail-featurelist">
								<?php if(have_rows('featurelist')): ?>
								<?php
									while(have_rows('featurelist')):
										the_row();?>
								<?php
										 $icon = get_sub_field('featureicon');
										 $txt = get_sub_field('featuretxt');
								?>

								<?php	if($icon){ ?>
								<li class="proLP-detail-featurelist__item">
									<img src="<?php echo $icon;?>">
									<p><?php echo $txt; ?></p>
								</li>

							<?php } else { ?>

								<li class="proLP-detail-featurelist__item">
									<img src="<?php echo get_stylesheet_directory_uri();?>/img/product/icon-pro-check.png">
									<p><?php echo $txt; ?></p>
								</li>

							<?php } ?>

						<?php endwhile;
								else : ?>

									<!--no content　-->

	<?php endif; ?>

							</ul>

				    </div><!-- ./textarea -->

		</div><!-- .proLP-inner-wrap -->
		<!--  -->
		<div class="proLP-inner-wrap flexorder">

			<div class="proLP-detail-pricearea">
				<h3 class="proLP-detail-box-ttl">価格</h3>
				<!-- <p>オープン価格</p> -->
				<?php
				$pricearea = get_field('pricearea');
				if($pricearea){ ?>
					<p><?php echo $pricearea; ?></p>
				<?php } ?>

				<h3 class="proLP-detail-box-ttl">製品仕様</h3>
				<?php
				$specarea = get_field('specarea');
				if($specarea){ ?><p><?php echo $specarea; ?></p>
				<?php } ?>

			</div><!-- ./pricearea -->



			<div class="proLP-detail-contactarea">
			<!--ここからお問い合わせbtn-->
					<?php
					$btnContact = have_rows('btnContact');
				if($btnContact){
					 while ( have_rows('btnContact') ) {
						 the_row();
						 $btnContacttxt = get_sub_field('btnContact-txt');
						 $btnContacturl = get_sub_field('btnContact-url');
						//  $btnContactblank = get_sub_field('btnContact-blank');
				?>

					<a target="<?php echo get_sub_field( 'btnContact-blank', false, false ) == true ? '_blank' : '_self'; ?>" href="<?php echo $btnContacturl; ?>" class="btn-contact" target="_blank"><?php echo $btnContacttxt; ?></a>
			<?php	}
			 } ?>
			 <!--./ ここまでお問い合わせbtn-->

			 <!--動画youtube -->
		 	<?php
		 	$detailarea = get_field('detailarea');
		 	if($detailarea){ ?>
		 		<div class="proLP-detail-textarea-desc"><?php echo $detailarea; ?></div>
		 	<?php } ?>
		 	<!-- -->

		 </div><!-- ./contactarea -->




		</div><!-- .proLP-inner-wrap -->
		<!-- -->
		<div class="proLP-inner-wrap">


		 <div class="proLP-detail-pimg2">
			 <?php the_content(); ?>
		 </div>

		 <?php
		 $dlistbox = have_rows('dlistbox');
		 if($dlistbox){ ?>
		<div class="proLP-detail-dlistbox">
							<h3 class="proLP-detail-dlistbox-title">ダウンロード</h3>
							<ul class="proLP-detail-dlistbox-list">


<?php

									while ( have_rows('dlistbox') ) {
										the_row();
										$dlistitem = get_sub_field('dlistitem');
										$dlisttxt = get_sub_field('dlisttxt');
										?>
										<li class="proLP-detail-dlistbox-list-item">
											<a href="<?php echo $dlistitem; ?>" target="_blank"><?php echo $dlisttxt; ?></a></li>
										<?php	}
									} ?>
								</ul>
				</div>



						<!-- custom field ACF ver. -->
						<?php $relate = get_field('related-post'); ?>
						<?php if($relate): ?>
							<hr/>
							<section class="detail-relation">
								<h3 class="detail-relation-tll">関連コンテンツ</h4>
									<dl class="detail-relation-list">
										<?php foreach((array)$relate as $value):?>

											<a href="<?php echo get_the_permalink($value->ID); ?>" rel="bookmark" title="<?php echo $value->post_title; ?>">
												<dt>
													<div class="detail-relation-thumbnail"><?php echo get_the_post_thumbnail($value->ID,array( 290, 170 )); ?></div>
												</dt>
												<dd>
													<span class="detail-relation-data">
														<time datetime="<?php echo get_the_date( 'Y-m-d',$value->ID) ?>"><?php echo get_the_time('Y-m-d',$value->ID) ?></time></span>
														<h4 class="detail-relation-text"><?php echo $value->post_title; ?></h4>
													</dd>
												</a>
											<?php endforeach; ?>
										</dl>
									</section>
								<?php endif; ?>
								<!-- ./custom field ACF ver. -->
				</div><!-- .proLP-inner-wrap -->



			</article><!-- #post-## -->



		<?php endwhile;
		?>



		</main><!-- #main -->
	</div><!-- #primary -->


<?php
get_footer();
