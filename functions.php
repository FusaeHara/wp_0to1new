<?php
/**
 * zero_to_one functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package zero_to_one
 */

if ( ! function_exists( 'zero_to_one_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function zero_to_one_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on zero_to_one, use a find and replace
	 * to change 'zero_to_one' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'zero_to_one', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'menu-1' => esc_html__( 'Primary', 'zero_to_one' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'zero_to_one_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif;
add_action( 'after_setup_theme', 'zero_to_one_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function zero_to_one_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'zero_to_one_content_width', 640 );
}
add_action( 'after_setup_theme', 'zero_to_one_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function zero_to_one_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'zero_to_one' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'zero_to_one' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'zero_to_one_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function zero_to_one_scripts() {
	wp_enqueue_style( 'zero_to_one-style', get_stylesheet_uri() );

	wp_enqueue_style( 'zero_to_one-fa',get_template_directory_uri().'/font-awesome.min.css' );

	//swiper
	wp_enqueue_style( 'zero_to_one-swiper',get_template_directory_uri().'/css/swiper.min.css' );

	//remodal
	wp_enqueue_style( 'zero_to_one-remodalcss',get_template_directory_uri().'/css/remodal.css' );
	wp_enqueue_style( 'zero_to_one-remodal-theme',get_template_directory_uri().'/css/remodal-default-theme.css' );

	// wp_enqueue_script( 'zero_to_one-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	// wp_enqueue_script( 'zero_to_one-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
 	//wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'zero_to_one-jquery', get_template_directory_uri() . '/js/app.js', array('jquery'));
	wp_enqueue_script( 'zero_to_one-masonry', 'https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js', array(), NULL , true );

	//swiper
	wp_enqueue_script( 'swiperjs',get_template_directory_uri().'/js/swiper.min.js');

	//remodal
	wp_enqueue_script( 'remodaljs',get_template_directory_uri().'/js/remodal.min.js');


	//bxslider for toppage

	wp_enqueue_style( 'zero_to_one-bxslider','https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css' );
	wp_enqueue_script( 'zero_to_one-bxslider', 'https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js', array(), NULL , true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'zero_to_one_scripts' );

/**
 * wp_footerでJSをロード
 */
 function my_load_widget_scripts() {
	 	//googlefontを読み込み
     wp_enqueue_script('widgets_js', '//platform.twitter.com/widgets.js', array());
     // Twitterウィジェット用のJSの読み込み
     wp_enqueue_script('widgets_js', '//platform.twitter.com/widgets.js', array());
 }
 // wp_footerに処理を登録
 add_action('wp_footer', 'my_load_widget_scripts');


 /**
  * 「メディア」の移動
  */
 function customize_menus(){
 global $menu;
 $menu[19] = $menu[10];  //メディアの移動
 unset($menu[10]);
 }
 add_action( 'admin_menu', 'customize_menus' );



 /**
  * 資料一覧ページ用の投稿タイプを生成
  */
 $args = array(
     'label' => '資料ダウンロード',//投稿タイプの名前
     'labels' => array(
         'singular_name' => '資料ダウンロード',//投稿タイプの名前
         'menu_name' => '資料ダウンロード',//メニュー（画面の左）に表示するラベル
         'add_new_item' => '新規投稿を追加',//新規作成ページの左上に表示されるタイトル
         'add_new' => '新規追加',//メニュー（画面の左）の「新規」の位置に表示するラベル
         'new_item' => '新規投稿',//一覧ページの右上にある新規作成ボタンのラベル
         'edit_item'=>'投稿を編集',//編集ページの左上にあるタイトル
         'view_item' => '投稿を表示',//編集ページの「○○を表示」ボタンのラベル
         'not_found' => '投稿は見つかりませんでした',//カスタム投稿を追加していない状態で、カスタム投稿一覧ページを開いたときに表示されるメッセージ
         'not_found_in_trash' => 'ゴミ箱に投稿はありません。',//カスタム投稿をゴミ箱に入れていない状態で、カスタム投稿のゴミ箱ページを開いたときに表示されるメッセージ
         'search_items' => '投稿を検索',//一覧ページの検索ボタンのラベル
     ),

     'public' => true,//ユーザーが管理画面で入力するか設定
     'publicly_queryable' => true,//カスタム投稿タイプの機能でページを生成するかどうかを指定
     'show_ui' => true,//管理画面にこのカスタム投稿タイプのページを表示するか設定
     'show_in_menu' => true,//管理画面にメニュー出すか設定
     'query_var' => true,
     'has_archive' => true,//「true」に指定すると投稿した記事の一覧ページ（投稿タイプのトップページ）を作成することができる
     'hierarchical' => false,//カスタム投稿に固定ページのような親子関係（階層）を持たせるか設定
     'menu_position' => 5,//カスタム投稿のメニューを追加する位置を整数で指定
     'rewrite' => true,//リライト設定
 );
 register_post_type("downloads",$args);


 /**
  * トップ　new ４つの事業バナー用の投稿タイプを生成
  */
 $args = array(
 	 'label' => '4事業バナー',//投稿タイプの名前
 	 'labels' => array(
 			 'singular_name' => '4事業バナー',//投稿タイプの名前
 			 'menu_name' => '4事業バナー',//メニュー（画面の左）に表示するラベル
 			 'add_new_item' => '新規画像を追加',//新規作成ページの左上に表示されるタイトル
 			 'add_new' => '新規追加',//メニュー（画面の左）の「新規」の位置に表示するラベル
 			 'new_item' => '新規投稿',//一覧ページの右上にある新規作成ボタンのラベル
 			 'edit_item'=>'投稿を編集',//編集ページの左上にあるタイトル
 			 'view_item' => '投稿を表示',//編集ページの「○○を表示」ボタンのラベル
 			 'not_found' => '投稿は見つかりませんでした',//カスタム投稿を追加していない状態で、カスタム投稿一覧ページを開いたときに表示されるメッセージ
 			 'not_found_in_trash' => 'ゴミ箱に投稿はありません。',//カスタム投稿をゴミ箱に入れていない状態で、カスタム投稿のゴミ箱ページを開いたときに表示されるメッセージ
 			 'search_items' => '投稿を検索',//一覧ページの検索ボタンのラベル
 	 ),

 	 'public' => true,//ユーザーが管理画面で入力するか設定
 	 'publicly_queryable' => true,//カスタム投稿タイプの機能でページを生成するかどうかを指定
 	 'show_ui' => true,//管理画面にこのカスタム投稿タイプのページを表示するか設定
 	 'show_in_menu' => true,//管理画面にメニュー出すか設定
 	 'query_var' => true,
 	 'has_archive' => true,//「true」に指定すると投稿した記事の一覧ページ（投稿タイプのトップページ）を作成することができる
 	 'hierarchical' => false,//カスタム投稿に固定ページのような親子関係（階層）を持たせるか設定
 	 'menu_position' => 5,//カスタム投稿のメニューを追加する位置を整数で指定
 	 'rewrite' => true,//リライト設定
 );
 register_post_type("stickbnr",$args);


 /**
  * トップ　new イベント特設バナー用の投稿タイプを生成
  */
 $args = array(
 	 'label' => '特設バナー',//投稿タイプの名前
 	 'labels' => array(
 			 'singular_name' => '特設バナー',//投稿タイプの名前
 			 'menu_name' => '特設バナー',//メニュー（画面の左）に表示するラベル
 			 'add_new_item' => '新規バナーを追加',//新規作成ページの左上に表示されるタイトル
 			 'add_new' => '新規追加',//メニュー（画面の左）の「新規」の位置に表示するラベル
 			 'new_item' => '新規投稿',//一覧ページの右上にある新規作成ボタンのラベル
 			 'edit_item'=>'投稿を編集',//編集ページの左上にあるタイトル
 			 'view_item' => '投稿を表示',//編集ページの「○○を表示」ボタンのラベル
 			 'not_found' => '投稿は見つかりませんでした',//カスタム投稿を追加していない状態で、カスタム投稿一覧ページを開いたときに表示されるメッセージ
 			 'not_found_in_trash' => 'ゴミ箱に投稿はありません。',//カスタム投稿をゴミ箱に入れていない状態で、カスタム投稿のゴミ箱ページを開いたときに表示されるメッセージ
 			 'search_items' => '投稿を検索',//一覧ページの検索ボタンのラベル
 	 ),

 	 'public' => true,//ユーザーが管理画面で入力するか設定
 	 'publicly_queryable' => true,//カスタム投稿タイプの機能でページを生成するかどうかを指定
 	 'show_ui' => true,//管理画面にこのカスタム投稿タイプのページを表示するか設定
 	 'show_in_menu' => true,//管理画面にメニュー出すか設定
 	 'query_var' => true,
 	 'has_archive' => true,//「true」に指定すると投稿した記事の一覧ページ（投稿タイプのトップページ）を作成することができる
 	 'hierarchical' => false,//カスタム投稿に固定ページのような親子関係（階層）を持たせるか設定
 	 'menu_position' => 5,//カスタム投稿のメニューを追加する位置を整数で指定
 	 'rewrite' => true,//リライト設定
 );
 register_post_type("seasonalbnr",$args);


 /**
  * トップ　new コンバージョンバナー用の投稿タイプを生成
  */
 $args = array(
 	 'label' => 'CVバナー',//投稿タイプの名前
 	 'labels' => array(
 			 'singular_name' => 'CVバナー',//投稿タイプの名前
 			 'menu_name' => 'CVバナー',//メニュー（画面の左）に表示するラベル
 			 'add_new_item' => '新規バナーを追加',//新規作成ページの左上に表示されるタイトル
 			 'add_new' => '新規追加',//メニュー（画面の左）の「新規」の位置に表示するラベル
 			 'new_item' => '新規投稿',//一覧ページの右上にある新規作成ボタンのラベル
 			 'edit_item'=>'投稿を編集',//編集ページの左上にあるタイトル
 			 'view_item' => '投稿を表示',//編集ページの「○○を表示」ボタンのラベル
 			 'not_found' => '投稿は見つかりませんでした',//カスタム投稿を追加していない状態で、カスタム投稿一覧ページを開いたときに表示されるメッセージ
 			 'not_found_in_trash' => 'ゴミ箱に投稿はありません。',//カスタム投稿をゴミ箱に入れていない状態で、カスタム投稿のゴミ箱ページを開いたときに表示されるメッセージ
 			 'search_items' => '投稿を検索',//一覧ページの検索ボタンのラベル
 	 ),

 	 'public' => true,//ユーザーが管理画面で入力するか設定
 	 'publicly_queryable' => true,//カスタム投稿タイプの機能でページを生成するかどうかを指定
 	 'show_ui' => true,//管理画面にこのカスタム投稿タイプのページを表示するか設定
 	 'show_in_menu' => true,//管理画面にメニュー出すか設定
 	 'query_var' => true,
 	 'has_archive' => true,//「true」に指定すると投稿した記事の一覧ページ（投稿タイプのトップページ）を作成することができる
 	 'hierarchical' => false,//カスタム投稿に固定ページのような親子関係（階層）を持たせるか設定
 	 'menu_position' => 5,//カスタム投稿のメニューを追加する位置を整数で指定
 	 'rewrite' => true,//リライト設定
 );
 register_post_type("cvbnr",$args);



 /**
  * トップ　メインバナー用の投稿タイプを生成
  */
 // $args = array(
 // 		'label' => 'メインバナー',//投稿タイプの名前
 // 		'labels' => array(
 // 				'singular_name' => 'メインバナー',//投稿タイプの名前
 // 				'menu_name' => 'メインバナー',//メニュー（画面の左）に表示するラベル
 // 				'add_new_item' => '新規バナーを追加',//新規作成ページの左上に表示されるタイトル
 // 				'add_new' => '新規追加',//メニュー（画面の左）の「新規」の位置に表示するラベル
 // 				'new_item' => '新規投稿',//一覧ページの右上にある新規作成ボタンのラベル
 // 				'edit_item'=>'投稿を編集',//編集ページの左上にあるタイトル
 // 				'view_item' => '投稿を表示',//編集ページの「○○を表示」ボタンのラベル
 // 				'not_found' => '投稿は見つかりませんでした',//カスタム投稿を追加していない状態で、カスタム投稿一覧ページを開いたときに表示されるメッセージ
 // 				'not_found_in_trash' => 'ゴミ箱に投稿はありません。',//カスタム投稿をゴミ箱に入れていない状態で、カスタム投稿のゴミ箱ページを開いたときに表示されるメッセージ
 // 				'search_items' => '投稿を検索',//一覧ページの検索ボタンのラベル
 // 		),
 //
 // 		'public' => true,//ユーザーが管理画面で入力するか設定
 // 		'publicly_queryable' => true,//カスタム投稿タイプの機能でページを生成するかどうかを指定
 // 		'show_ui' => true,//管理画面にこのカスタム投稿タイプのページを表示するか設定
 // 		'show_in_menu' => true,//管理画面にメニュー出すか設定
 // 		'query_var' => true,
 // 		'has_archive' => true,//「true」に指定すると投稿した記事の一覧ページ（投稿タイプのトップページ）を作成することができる
 // 		'hierarchical' => false,//カスタム投稿に固定ページのような親子関係（階層）を持たせるか設定
 // 		'menu_position' => 5,//カスタム投稿のメニューを追加する位置を整数で指定
 // 		'rewrite' => true,//リライト設定
 // );
 // register_post_type("mainbnr",$args);


 /**
	* トップ　サブバナー用の投稿タイプを生成
	*/
 // $args = array(
	// 	 'label' => 'サブバナー',//投稿タイプの名前
	// 	 'labels' => array(
	// 			 'singular_name' => 'サブバナー',//投稿タイプの名前
	// 			 'menu_name' => 'サブバナー',//メニュー（画面の左）に表示するラベル
	// 			 'add_new_item' => '新規バナーを追加',//新規作成ページの左上に表示されるタイトル
	// 			 'add_new' => '新規追加',//メニュー（画面の左）の「新規」の位置に表示するラベル
	// 			 'new_item' => '新規投稿',//一覧ページの右上にある新規作成ボタンのラベル
	// 			 'edit_item'=>'投稿を編集',//編集ページの左上にあるタイトル
	// 			 'view_item' => '投稿を表示',//編集ページの「○○を表示」ボタンのラベル
	// 			 'not_found' => '投稿は見つかりませんでした',//カスタム投稿を追加していない状態で、カスタム投稿一覧ページを開いたときに表示されるメッセージ
	// 			 'not_found_in_trash' => 'ゴミ箱に投稿はありません。',//カスタム投稿をゴミ箱に入れていない状態で、カスタム投稿のゴミ箱ページを開いたときに表示されるメッセージ
	// 			 'search_items' => '投稿を検索',//一覧ページの検索ボタンのラベル
	// 	 ),
 //
	// 	 'public' => true,//ユーザーが管理画面で入力するか設定
	// 	 'publicly_queryable' => true,//カスタム投稿タイプの機能でページを生成するかどうかを指定
	// 	 'show_ui' => true,//管理画面にこのカスタム投稿タイプのページを表示するか設定
	// 	 'show_in_menu' => true,//管理画面にメニュー出すか設定
	// 	 'query_var' => true,
	// 	 'has_archive' => true,//「true」に指定すると投稿した記事の一覧ページ（投稿タイプのトップページ）を作成することができる
	// 	 'hierarchical' => false,//カスタム投稿に固定ページのような親子関係（階層）を持たせるか設定
	// 	 'menu_position' => 5,//カスタム投稿のメニューを追加する位置を整数で指定
	// 	 'rewrite' => true,//リライト設定
 // );
 // register_post_type("topbnr",$args);

 /**
   * 電源IoT 投稿タイプを生成
   */
  $args = array(
  		'label' => '電源ページ',//投稿タイプの名前
  		'labels' => array(
  				'singular_name' => '電源ページ',//投稿タイプの名前
  				'menu_name' => '電源ページ',//メニュー（画面の左）に表示するラベル
  				'add_new_item' => '新規コラムを追加',//新規作成ページの左上に表示されるタイトル
  				'add_new' => '新規追加',//メニュー（画面の左）の「新規」の位置に表示するラベル
  				'new_item' => '新規投稿',//一覧ページの右上にある新規作成ボタンのラベル
  				'edit_item'=>'投稿を編集',//編集ページの左上にあるタイトル
  				'view_item' => '投稿を表示',//編集ページの「○○を表示」ボタンのラベル
  				'not_found' => '投稿は見つかりませんでした',//カスタム投稿を追加していない状態で、カスタム投稿一覧ページを開いたときに表示されるメッセージ
  				'not_found_in_trash' => 'ゴミ箱に投稿はありません。',//カスタム投稿をゴミ箱に入れていない状態で、カスタム投稿のゴミ箱ページを開いたときに表示されるメッセージ
  				'search_items' => '投稿を検索',//一覧ページの検索ボタンのラベル
  		),

  		'public' => true,//ユーザーが管理画面で入力するか設定
  		'publicly_queryable' => true,//カスタム投稿タイプの機能でページを生成するかどうかを指定
  		'show_ui' => true,//管理画面にこのカスタム投稿タイプのページを表示するか設定
  		'show_in_menu' => true,//管理画面にメニュー出すか設定
  		'query_var' => true,
  		'has_archive' => false,//「true」に指定すると投稿した記事の一覧ページ（投稿タイプのトップページ）を作成することができる
  		'hierarchical' => false,//カスタム投稿に固定ページのような親子関係（階層）を持たせるか設定
  		'menu_position' => 7,//カスタム投稿のメニューを追加する位置を整数で指定
  		'rewrite' => true,//リライト設定
  );
  register_post_type("supply",$args);

	/**
    * IoT 投稿タイプを生成
    */
   $args = array(
   		'label' => 'IoTページ',//投稿タイプの名前
   		'labels' => array(
   				'singular_name' => 'IoTページ',//投稿タイプの名前
   				'menu_name' => 'IoTページ',//メニュー（画面の左）に表示するラベル
   				'add_new_item' => '新規コラムを追加',//新規作成ページの左上に表示されるタイトル
   				'add_new' => '新規追加',//メニュー（画面の左）の「新規」の位置に表示するラベル
   				'new_item' => '新規投稿',//一覧ページの右上にある新規作成ボタンのラベル
   				'edit_item'=>'投稿を編集',//編集ページの左上にあるタイトル
   				'view_item' => '投稿を表示',//編集ページの「○○を表示」ボタンのラベル
   				'not_found' => '投稿は見つかりませんでした',//カスタム投稿を追加していない状態で、カスタム投稿一覧ページを開いたときに表示されるメッセージ
   				'not_found_in_trash' => 'ゴミ箱に投稿はありません。',//カスタム投稿をゴミ箱に入れていない状態で、カスタム投稿のゴミ箱ページを開いたときに表示されるメッセージ
   				'search_items' => '投稿を検索',//一覧ページの検索ボタンのラベル
   		),

   		'public' => true,//ユーザーが管理画面で入力するか設定
   		'publicly_queryable' => true,//カスタム投稿タイプの機能でページを生成するかどうかを指定
   		'show_ui' => true,//管理画面にこのカスタム投稿タイプのページを表示するか設定
   		'show_in_menu' => true,//管理画面にメニュー出すか設定
   		'query_var' => true,
   		'has_archive' => false,//「true」に指定すると投稿した記事の一覧ページ（投稿タイプのトップページ）を作成することができる
   		'hierarchical' => false,//カスタム投稿に固定ページのような親子関係（階層）を持たせるか設定
   		'menu_position' => 6,//カスタム投稿のメニューを追加する位置を整数で指定
   		'rewrite' => true,//リライト設定
   );
   register_post_type("iot",$args);


	/**
    * オンライン展示会 投稿タイプを生成
    */
   $args = array(
   		'label' => 'オンライン展示会',//投稿タイプの名前
   		'labels' => array(
   				'singular_name' => 'オンライン展示会',//投稿タイプの名前
   				'menu_name' => 'オンライン展示会',//メニュー（画面の左）に表示するラベル
   				'add_new_item' => '新規商品ページを追加',//新規作成ページの左上に表示されるタイトル
   				'add_new' => '新規追加',//メニュー（画面の左）の「新規」の位置に表示するラベル
   				'new_item' => '新規投稿',//一覧ページの右上にある新規作成ボタンのラベル
   				'edit_item'=>'投稿を編集',//編集ページの左上にあるタイトル
   				'view_item' => '投稿を表示',//編集ページの「○○を表示」ボタンのラベル
   				'not_found' => '投稿は見つかりませんでした',//カスタム投稿を追加していない状態で、カスタム投稿一覧ページを開いたときに表示されるメッセージ
   				'not_found_in_trash' => 'ゴミ箱に投稿はありません。',//カスタム投稿をゴミ箱に入れていない状態で、カスタム投稿のゴミ箱ページを開いたときに表示されるメッセージ
   				'search_items' => '投稿を検索',//一覧ページの検索ボタンのラベル
   		),

   		'public' => true,//ユーザーが管理画面で入力するか設定
   		'publicly_queryable' => true,//カスタム投稿タイプの機能でページを生成するかどうかを指定
   		'show_ui' => true,//管理画面にこのカスタム投稿タイプのページを表示するか設定
   		'show_in_menu' => true,//管理画面にメニュー出すか設定
   		'query_var' => true,
   		'has_archive' => true,//「true」に指定すると投稿した記事の一覧ページ（投稿タイプのトップページ）を作成することができる
   		'hierarchical' => true,//カスタム投稿に固定ページのような親子関係（階層）を持たせるか設定
   		'menu_position' => 7,//カスタム投稿のメニューを追加する位置を整数で指定
   		'rewrite' => true,//リライト設定
   );
   register_post_type("ol-expo",$args);


 /**
 	 * カスタム投稿の並び順を変更
	 */



/**
  * オンライン展示会　カテゴリー追加
  */

function add_taxonomies() {
    register_taxonomy(
				'expo_cat', //「works_cat」はお好みで変更してください
        'ol-expo', //「works」は作成したカスタム投稿タイプの名前にしてください
        array(
            'label' => 'ブースカテゴリー', //表示名
						'public' => true,
						'show_in_menu' => true,
            'show_ui' => true,
            'show_admin_column' => true,
            'show_in_nav_menus' => true,
            'hierarchical' => true, //trueはカテゴリー・falseはタグ
			'rewrite' => array( 'slug' => 'ol-expo/category/', 'with_front' => true, ),//パーマリンクの設定
			'show_in_rest' => true,
			'rest_base' => "",
			)
    );
}
add_action( 'init', 'add_taxonomies', 0 );



// function myUrlRewrite(){
// 	add_rewrite_rule('ol-expo/([^/]+)/?$', 'index.php?expo_cat=$matches[1]', 'top');
// 	}
// add_action( 'init', 'myUrlRewrite' );


function myUrlRewrite($rules){
$myRule = array();
$myRule['ol-expo/category/([^/]+)/?$'] = 'index.php?expo_cat=$matches[1]';
return array_merge( $myRule, $rules );
}
add_action('rewrite_rules_array', 'myUrlRewrite' );


/**
 * 「カテゴリー：」表記をリセット
*/
function custom_archive_title($title){
    $titleParts=explode(': ',$title);
    if($titleParts[1]){
        return $titleParts[1];
    }
    return $title;
}
add_filter('get_the_archive_title','custom_archive_title');



 /**
	 * 画像幅リセット
 */

add_filter('image_send_to_editor', 'remove_image_attribute', 10);
add_filter('post_thumbnail_html', 'remove_image_attribute', 10);
function remove_image_attribute($html){
  $html = preg_replace('/(width|height)="\d*"\s/', '', $html); // width と heifht を削除
  $html = preg_replace('/class=[\'"]([^\'"]+)[\'"]/i', '', $html); // class を削除
  $html = preg_replace('/title=[\'"]([^\'"]+)[\'"]/i', '', $html); // title を削除
  $html = preg_replace('/<a href=".+">/', '', $html); // a タグを削除
  $html = preg_replace('/<\/a>/', '', $html); // a の閉じタグのを削除
  return $html;
}



/**
*	 アイキャッチ表示されない問題
*/
add_theme_support('post-thumbnails');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';





/**
*  クロール解除
*/

/* インデックスと巡回の設定 */
function add_noindex_insert(){

	/* 特定の固定ページの場合 */
	if (is_page(array('supply','iot'))) {
		echo '<meta name="robots" content="noindex,nofollow,noarchive">'."\n";
		echo '<meta name="googlebot" content="noindex,nofollow,noarchive">'."\n";
	}

}
add_action('wp_head', 'add_noindex_insert');
