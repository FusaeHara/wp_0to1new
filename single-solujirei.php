<?php
/**
 * The template for displaying all solution posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package zero_to_one
 *
 * WP Post Template: ソリューション事例テンプレート
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<!--pankuzuここから-->
			<?php
				$cat = get_the_category();
				$cat = $cat[0];

				//親カテゴリがあるか
				if($cat->category_parent){
					$parent_cat = get_category($cat->category_parent);
					$catname = $parent_cat->name;
					$catslug = $parent_cat->slug;
				}else{
					$catname = $cat->name;//カテゴリー名
					$catslug = $cat->slug;//スラッグ名
				}

			?>
				<div class="pankuzu">
					<ol class="pankuzu-list">
						<li><a href="<?php echo home_url();?>">ホーム</a></li>
						<li><a href="<?php echo home_url();?>/category/<?php echo $catslug;?>/"><?php echo $catname;?></a></li>
						<li><?php echo get_the_title();?></li>
					</ol>
				</div>
			<!--pankuzuここまで-->
			<div class="jireiLP-detail-wrapper">

		<?php
		while ( have_posts() ) : the_post();?>
			<article id="post-<?php the_ID(); ?>" <?php post_class('jirei-area-2'); ?>>

					<?php
					//タイトル表示
					if ( is_single() ) :
						the_title( '<h1 class="jireiLP-detail-ttl"><div class="detail-label solution card-article-label--jirei">納入事例</div>', '</h1>' );
					else :
						the_title( '<h2 class="jireiLP-detail-ttl"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
					endif; ?>


				<div class="jireiLP-inner-vi">
         <div class="jireiLP-inner-vi-main">
					<?php
					if ( has_post_thumbnail() ) { //アイキャッチ画像表示
						the_post_thumbnail();
					} ?>

					<?php
						$herocom = get_field('hero-comment');  //アイキャッチ画像のコメント
							if($herocom){ ?>
								<p class="jireiLP-detail-herocomment">
									<?php echo $herocom; ?>
								</p>
						<?php } ?>
					</div>
						<div class="jireiLP-inner-vi-sub">

							<?php
								$ar1title = get_field('jirei-area1-title');  //エリア１のタイトル
								$ar1desc = get_field('jirei-area1-desc');  //エリア１の施設名〜担当者名

									if($ar1title){ ?>
										<h2 class="jireiLP-inner-vi-sub-title">
											<?php echo $ar1title; ?>
										</h2>
										<p class="jireiLP-inner-vi-sub-desc">
											<?php echo $ar1desc; ?>
										</p>
								<?php } ?>


								<?php
									$ar1prof = get_field('jirei-area1-prof');  //COMPANY PROFILE（任意）

										if($ar1prof){ ?>
											<h3 class="jireiLP-inner-vi-prof-title">
											<i class="fa fa-building"></i>
											COMPANY PROFILE
											</h3>

    								<p class="jireiLP-inner-vi-prof-desc">
											<?php echo $ar1prof; ?>
</p>
<?php } ?>
				</div>
				</div>


				<div class="jireiLP-inner-wrap">

					<?php
					$colums = have_rows('repeat-solphotos');
					if($colums){
						 while ( have_rows('repeat-solphotos') ) {
							 the_row();
							$imgurl = get_sub_field('solphoto');
							$txt = get_sub_field('soltxt');
					?>
							<div class="jireiLP-detail-pimg-wrap" style="margin-bottom:30px;">
								<img src="<?php echo $imgurl; ?>">
								<p class="jireiLP-detail-herocomment"><?php echo $txt; ?></p>
							</div>

						<?php	}
						 } ?>





				<div class="jireiLP-detail-pimg-wrap">
					  <div class="jireiLP-detail-pimg">
							<?php the_content(); //スライダー写真　?>
			  		</div>
        </div>

						<div class="jireiLP-detail-textarea">

							<?php
							$colums = have_rows('repeat-colums');
							if($colums){
								 while ( have_rows('repeat-colums') ) {
									 the_row();
									$title = get_sub_field('re-title');
									$txt = get_sub_field('re-txt');
							?>

								<div class="jireiLP-detail-textarea-inner">
									<h3 class="jireiLP-detail-textarea-inner-title"><?php echo $title; ?></h3>
									<p class="jireiLP-detail-textarea-inner-txt"><?php echo $txt; ?></p>
								</div>


						<?php	}
						 } ?>

				    </div>
					</div><!-- ./textarea -->


						<!--使用商品バナー一覧-->

						<div class="jireiLP-detail-bnrlist">
							<h3 class="jireiLP-detail-bnrlist-title">
								使用商品
							</h3>
							<ul class="jireiLP-detail-bnrlist-inner">

									<?php
									$colums = have_rows('repeat-bnr');
									if($colums){
										 while ( have_rows('repeat-bnr') ) {
											 the_row();
											$imgurl = get_sub_field('repeat-bnr-img');
											$url = get_sub_field('repeat-bnr-url');
											$alt = get_sub_field('repeat-bnr-alt');
									?>

									<!--使用商品バナー 単品-->
											<li class="jireiLP-detail-bnrlist-item">
												<a href="<?php echo $url; ?>">
												<img src="<?php echo $imgurl; ?>" alt="<?php echo $alt; ?>">
												</a>
											</li>
									<!-- ./使用商品バナー 単品-->


									<?php	}
									 } ?>


							</ul>
						</div>
						<!-- ./使用商品バナー一覧-->





						</div><!-- ./jireiLP-inner-wrap -->

						<hr/ class="jireiLP-detail-wrapper">


						<!-- custom field ACF ver. -->
						<?php $relate = get_field('related-post'); ?>
						<?php if($relate): ?>
							<section class="detail-relation jireiLP-detail-wrapper">
								<h3 class="detail-relation-tll jireiLP-detail-margintop">関連コンテンツ</h4>
									<dl class="detail-relation-list">
										<?php foreach((array)$relate as $value):?>

											<a href="<?php echo get_the_permalink($value->ID); ?>" rel="bookmark" title="<?php echo $value->post_title; ?>">
												<dt>
													<div class="detail-relation-thumbnail"><?php echo get_the_post_thumbnail($value->ID,array( 290, 170 )); ?></div>
												</dt>
												<dd>
													<span class="detail-relation-data">
														<time datetime="<?php echo get_the_date( 'Y-m-d',$value->ID) ?>"><?php echo get_the_time('Y-m-d',$value->ID) ?></time></span>
														<h4 class="detail-relation-text"><?php echo $value->post_title; ?></h4>
													</dd>
												</a>
											<?php endforeach; ?>
										</dl>
									</section>
								<?php endif; ?>
								<!-- ./custom field ACF ver. -->
				</div><!-- .proLP-inner-wrap -->




			</article><!-- #post-## -->

		</div><!-- .jireiLP-detail-wrapper -->

		<?php endwhile;
		?>



		</main><!-- #main -->
	</div><!-- #primary -->


<?php
get_footer();
