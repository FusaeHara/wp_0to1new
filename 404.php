<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package zero_to_one
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">

				<div class="page-content">

					<h1 class="categorypage-ttl"><strong>404 ERROR</strong></h1>

					<div class="inner-wrap--min">
						<h2>ページが見つかりません</h2>
						<p>ご指定のURLは、このサーバでは見つかりませんでした。</p>
						<p>該当URLが変更になったか、あるいはタイプミスの可能性があります。</p>
						<p>URLをご確認の上、もう一度お試し下さい。</p>
					</div>


				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
