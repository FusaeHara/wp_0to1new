<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package zero_to_one
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<aside id="secondary" class="sidebar" role="complementary">

	<!--<?php dynamic_sidebar( 'sidebar-1' ); ?>-->

			<div class="sidebar-banner-area">
			<?php if(in_category('p-pest')) : ?>
			<div class="sidebar-banner">
				<a href="https://www.facebook.com/groups/1308280099264158/?notif_t=group_added_to_group&notif_id=1492569890113462"><img src="https://www.luci.co.jp/jp/zero_to_one/wp-content/uploads/2017/05/community_s.png" target="_blank" alt=""></a>
			</div>
			<?php endif; ?>

			<?php if(in_category('s-pest')) : ?>
			<div class="sidebar-banner">
			<a href="https://www.facebook.com/groups/1308280099264158/?notif_t=group_added_to_group&notif_id=1492569890113462"><img src="https://www.luci.co.jp/jp/zero_to_one/wp-content/uploads/2017/05/community_s.png" target="_blank" alt=""></a>
			</div>
			<?php endif; ?>


	<!--キーワードエリア-->
	    <div id="keyword_content" class="relation-tag">
	        <h3 class="relation-tag-ttl">
					0 to 1 Keywords ///
					</h3>
	        <div class="relation-tag-list">
						<?php $args = array(
							'smallest'                  => 10,
							'largest'                   => 10,
							'number'                    => 50,
							'format'										=> 'list',
							'separator'                 => "\n",
							'orderby'                   => 'name',
							'order'                     => 'ASC',
							'exclude'                   => null,
							'link'                      => 'view',
							'taxonomy'                  => 'post_tag',
							'echo'                      => true,
						); ?>
						<?php wp_tag_cloud( $args ); ?>
					</div>
			</div>
			<!--/キーワードエリアend-->



</aside><!-- #secondary -->
