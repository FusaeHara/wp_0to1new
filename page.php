<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package zero_to_one
 */

get_header(); ?>

			<!--pankuzuここから-->
				<div class="pankuzu">
					<ol class="pankuzu-list">
						<li><a href="<?php echo home_url();?>">ホーム</a></li>
						<li><?php echo get_the_title(); ?></li>
					</ol>
				</div>
			<!--pankuzuここまで-->

			<h1 class="categorypage-ttl"><strong><?php echo get_the_title(); ?></strong></h1>

			<section class="inner-wrap--min">
			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

			endwhile; // End of the loop.
			?>

			</section><!-- ./inner-wrap-min-->

<?php
get_footer();
