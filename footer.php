<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package zero_to_one
 */

?>

	</div><!-- #content -->

	<?php wp_reset_query(); ?>
	<?php	if ( is_front_page() && is_home() ) : ?>
	<?php else : ?>
	<section class="low-banner flag">
      <?php get_template_part( 'template-parts/seasonal-low','banner' ); ?>
	</section>
	<?php endif;?>

			<footer id="colophon" class="footer" role="contentinfo">

				<div class="footer-logo"><a href="https://www.luci.co.jp/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri();?>/img/common/luci2.svg" alt=""></a></div>
				<p class="copyright">©株式会社Luci  ソリューション事業部</p>

				<section class="sns-area">
					<div class="sns-btn sns-tw">
						<a href="https://twitter.com/share" class="twitter-share-button" data-url="https://www.luci.co.jp/jp/zero_to_one/" data-hashtags="zero-to-one">Tweet</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
					</div>
					<div class="sns-btn sns-fb">
						<div class="fb-like" data-href="https://www.luci.co.jp/jp/zero_to_one/" data-layout="button" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
					</div>
					<div class="sns-btn sns-gplus">
						<div class="g-plusone" data-annotation="none" data-href="https://www.luci.co.jp/jp/zero_to_one/"></div>
					</div>
					<div class="sns-btn sns-line">
		        <div class="line-it-button" data-lang="ja" data-type="share-a" data-url="https://www.luci.co.jp/jp/zero_to_one/" style="display: none;"></div>
		        <script src="https://d.line-scdn.net/r/web/social-plugin/js/thirdparty/loader.min.js" async="async" defer="defer"></script>
					</div>
				</section>
			</footer><!-- #colophon -->



</div><!-- #page -->

<a href="#" class="pagetop"><img src="<?php echo get_stylesheet_directory_uri();?>/img/common/pagetop.svg" alt=""></a>

<?php wp_footer(); ?>
<script src="https://apis.google.com/js/platform.js" async defer>
  {lang: 'ja'}
</script>


</body>
</html>
