<?php
/**
 * Template part for displaying 4 sub banner
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package zero_to_one
 */

?>

<ul class="low-banner-list flexbox">

<?php  query_posts( array(
'post_type'=>'seasonalbnr',
'post_status' => 'publish',
'paged' => get_query_var('paged')
) );
?>

<?php if (have_posts()) : while (have_posts()) : the_post();
?>

  <?php if( get_field('seasonalbnr-blank')=="inner"): ?>
    <li><a href="<?php the_field('seasonalbnr-url');?>"><div><img src="<?php the_field('seasonalbnr-img'); ?>" alt="<?php the_field('seasonalbnr-desc');?>" /></div></a></li>
  <?php elseif( get_field('seasonalbnr-blank')=="outside"): ?>
    <li><a href="<?php the_field('seasonalbnr-url');?>" target="_blank"><div><img src="<?php the_field('seasonalbnr-img'); ?>" alt="<?php the_field('seasonalbnr-desc');?>" /></div></a></li>
  <?php endif; ?>

<?php endwhile; endif; ?>
<?php wp_reset_postdata();?>


<?php  query_posts( array(
'post_type'=>'cvbnr',
'post_status' => 'publish',
'paged' => get_query_var('paged')
) );
?>

<?php if (have_posts()) : while (have_posts()) : the_post();
?>

  <?php if( get_field('cvbnr-blank')=="inner"): ?>
    <li><a href="<?php the_field('cvbnr-url');?>"><div><img src="<?php the_field('cvbnr-img'); ?>" alt="<?php the_field('cvbnr-desc');?>" /></div></a></li>
  <?php elseif( get_field('cvbnr-blank')=="outside"): ?>
    <li><a href="<?php the_field('cvbnr-url');?>" target="_blank"><div><img src="<?php the_field('cvbnr-img'); ?>" alt="<?php the_field('cvbnr-desc');?>" /></div></a></li>
  <?php endif; ?>

<?php endwhile; endif; ?>
<?php wp_reset_postdata();?>

</ul>
