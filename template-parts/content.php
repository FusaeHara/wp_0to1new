<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package zero_to_one
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('detail-area'); ?>>
	<header class="detail-head">
		<?php
		//カテゴリと日付表示
		if ( 'post' === get_post_type() ) : ?>
			<?php zero_to_one_posted_detailinfo(); ?>
		<?php
		endif; ?>

		<?php
		//タイトル表示
		if ( is_single() ) :
			the_title( '<h1 class="detail-ttl">', '</h1>' );
		else :
			the_title( '<h2 class="detail-ttl"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif; ?>

		<?php
		//キーワードタグ表示
		if ( 'post' === get_post_type() ) : ?>
					<?php
				if ( get_the_tag_list() ) {
			    echo get_the_tag_list( '<ul class="detail-tag"><li>', '</li><li>', '</li></ul>' );
				}
			?>
		<?php
		endif; ?>

	</header><!-- .detail-head -->

	<div class="detail-content">

		<?php the_content(); ?>

	</div><!-- .detail-content -->

	<!-- custom field ACF ver. -->
	<?php $relate = get_field('related-post'); ?>
		<?php if($relate): ?>
			<section class="detail-relation">
				<h3 class="detail-relation-tll">関連コンテンツ</h4>
					<dl class="detail-relation-list">
					<?php foreach((array)$relate as $value):?>

						<a href="<?php echo get_the_permalink($value->ID); ?>" rel="bookmark" title="<?php echo $value->post_title; ?>">
							<dt>
								<div class="detail-relation-thumbnail"><?php echo get_the_post_thumbnail($value->ID,array( 290, 170 )); ?></div>
							</dt>
							<dd>
									<span class="detail-relation-data">
										<time datetime="<?php echo get_the_date( 'Y-m-d',$value->ID) ?>"><?php echo get_the_time('Y-m-d',$value->ID) ?></time></span>
									<h4 class="detail-relation-text"><?php echo $value->post_title; ?></h4>
							</dd>
						</a>
					<?php endforeach; ?>
				</dl>
			</section>
	<?php endif; ?>
	<!-- ./custom field ACF ver. -->


	<!--<footer class="entry-footer">
		<?php zero_to_one_entry_footer(); ?>
	</footer>--><!-- .entry-footer -->

</article><!-- #post-## -->
