<?php
/**
 * Template part for displaying 1 main-slider banner
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package zero_to_one
 */

?>


<!-- bxslider -->
<script>
    jQuery(document).ready(function($){
      $('.slider').bxSlider( {

        mode: 'fade',
        speed: 900,
        auto: true,
        easing: 'ease-out',
        slideMargin: 0,
        pager: true,
        controls: false,
        caption: true,
      }

      );
    });
</script>


<?php  query_posts( array(
'post_type'=>'mainbnr',
'post_status' => 'publish',
'paged' => get_query_var('paged')
) );
?>


<ul class="slider">

  <?php if (have_posts()) : while (have_posts()) : the_post();
  ?>

    <?php if( get_field('mainbnr-blank')=="inner"): ?>
      <li>
        <a href="<?php the_field('mainbnr-url');?>">
            <div class="slider-item" style="background-image:url('<?php the_field('mainbnr-img'); ?>');">
              <div class="slider-item--filter"></div><!--filter-->
            </div>

        </a>
      </li>
    <?php elseif( get_field('mainbnr-blank')=="outside"): ?>
      <li>
        <a href="<?php the_field('mainbnr-url');?>">
            <div class="slider-item" style="background-image:url('<?php the_field('mainbnr-img'); ?>');">
              <div class="slider-item--filter"></div><!--filter-->
            </div>
        </a>
      </li>
    <?php endif; ?>

  <?php endwhile; endif; ?>

</ul>
