<?php
/**
 * Template part for displaying 4 sub banner
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package zero_to_one
 */

?>

<ul class="sticker__list flexbox">

<?php  query_posts( array(
'post_type'=>'stickbnr',
'post_status' => 'publish',
'paged' => get_query_var('paged')
) );
?>



<?php if (have_posts()) : while (have_posts()) : the_post();
?>
    <li class="sticker__list__item"><a href="<?php the_field('stickbnr-url');?>"><div class="sticker__list__item__img"><img src="<?php the_field('stickbnr-img'); ?>" class="yellowmask"></div><p class="sticker__list__item__bar"><?php the_title(); ?></p></a></li>

<?php endwhile; endif; ?>

</ul>
