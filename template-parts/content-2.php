<?php
/**
 * Template part for displaying posts -- productinfo
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package zero_to_one
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('detail-area-2'); ?>>


	<div class="proLP-detail-head" style="background-image: url(<?php echo wp_get_attachment_url( 137 ); ?>)">

		<?php

		//タイトル表示
		if ( is_single() ) :
			$title = get_the_title();
 			$title = str_replace(" ", "<br />", $title);
 			echo '<h1 class="proLP-detail-ttl">'.$title.'</h1>';

		else :
			the_title( '<h2 class="proLP-detail-ttl"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif; ?>

	  <span class="proLP-detail-logo"><img src="../img/img_proLP-detail-logo.png" alt="虫とら"></span>

	</div><!-- ./detail-head-->


	<div class="proLP-inner-wrap">

		<?php
		//テキスト
		$txt = get_field('txt');
		if($txt){ ?>
			<p>テキスト：<? echo $txt; ?></p>
		<?php } ?>

		  <div class="proLP-detail-pimg">
				<?php the_content(); ?>
  		</div>


		<!-- ここにＡＣＦを入れる--->





	</div><!-- .proLP-inner-wrap -->

	<!-- custom field ACF ver. -->
	<?php $relate = get_field('related-post'); ?>
		<?php if($relate): ?>
			<section class="detail-relation">
				<h3 class="detail-relation-tll">関連コンテンツ</h4>
					<dl class="detail-relation-list">
					<?php foreach((array)$relate as $value):?>

						<a href="<?php echo get_the_permalink($value->ID); ?>" rel="bookmark" title="<?php echo $value->post_title; ?>">
							<dt>
								<div class="detail-relation-thumbnail"><?php echo get_the_post_thumbnail($value->ID,array( 290, 170 )); ?></div>
							</dt>
							<dd>
									<span class="detail-relation-data">
										<time datetime="<?php echo get_the_date( 'Y-m-d',$value->ID) ?>"><?php echo get_the_time('Y-m-d',$value->ID) ?></time></span>
									<h4 class="detail-relation-text"><?php echo $value->post_title; ?></h4>
							</dd>
						</a>
					<?php endforeach; ?>
				</dl>
			</section>
	<?php endif; ?>
	<!-- ./custom field ACF ver. -->


	<!--<footer class="entry-footer">
		<?php zero_to_one_entry_footer(); ?>
	</footer>--><!-- .entry-footer -->

</article><!-- #post-## -->
