<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package zero_to_one
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('detail-area'); ?>>

	<!--detai-headなし-->

	<div class="detail-content">

		<?php the_content(); ?>

	</div><!-- .detail-content -->


</article><!-- #post-## -->
