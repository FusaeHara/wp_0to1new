<?php
/**
 * Template part for displaying 4 sub banner
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package zero_to_one
 */

?>

<?php  query_posts( array(
'post_type'=>'cvbnr',
'post_status' => 'publish',
'paged' => get_query_var('paged')
) );
?>

<ul class="cvbnr__list">

<?php if (have_posts()) : while (have_posts()) : the_post();
?>

  <?php if( get_field('cvbnr-blank')=="inner"): ?>
    <li><a href="<?php the_field('cvbnr-url');?>"><img src="<?php the_field('cvbnr-img'); ?>" alt="<?php the_field('cvbnr-desc');?>"></a></li>
  <?php elseif( get_field('cvbnr-blank')=="outside"): ?>
    <li><a href="<?php the_field('cvbnr-url');?>" target="_blank"><img src="<?php the_field('cvbnr-img'); ?>" alt="<?php the_field('cvbnr-desc');?>"></a></li>
  <?php endif; ?>

<?php endwhile; endif; ?>

</ul>
