<?php
/**
 * Template part for displaying 4 sub banner
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package zero_to_one
 */

?>

<?php $cases_post_count = wp_count_posts('seasonalbnr')->publish;  //公開している記事数をカウント
if ( $cases_post_count != 0 ) :?>
<!-- バナーが1件でもあれば--->

<?php  query_posts( array(
'post_type'=>'seasonalbnr',
'post_status' => 'publish',
'paged' => get_query_var('paged')
) );
?>

<ul class="seasonalbnr__list">

<?php if (have_posts()) : while (have_posts()) : the_post();
?>

  <?php if( get_field('seasonalbnr-blank')=="inner"): ?>
    <li><a href="<?php the_field('seasonalbnr-url');?>"><img src="<?php the_field('seasonalbnr-img'); ?>" alt="<?php the_field('seasonalbnr-desc');?>"></a></li>
  <?php elseif( get_field('seasonalbnr-blank')=="outside"): ?>
    <li><a href="<?php the_field('seasonalbnr-url');?>" target="_blank"><img src="<?php the_field('seasonalbnr-img'); ?>" alt="<?php the_field('seasonalbnr-desc');?>"></a></li>
  <?php endif; ?>

<?php endwhile; endif; ?>

</ul>


<!-- ./バナーが1件でもあれば--->
<?php endif; ?>
