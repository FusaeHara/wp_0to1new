jQuery(function($){



  //共通
  jQuery(function($){
    jQuery('a[href^="#"]').click(function($) {
      var speed = 400;
      var href= jQuery(this).attr("href");
      var target = jQuery(href == "#" || href == "" ? 'html' : href);
      var position = target.offset().top -82;
      jQuery('body,html').animate({scrollTop:position}, speed, 'swing');
      return false;
    });
  });
  jQuery(window).on("scroll", function($){
  	if(jQuery(this).scrollTop() > 400){
  		jQuery('.header,.menu-trigger').addClass('up');
  	}else{
  		jQuery('.header').removeClass('up');
  	}
  });
  jQuery(document).ready(function($) {
    var pagetop = jQuery('.pagetop');
    jQuery(window).scroll(function ($) {
      if (jQuery(this).scrollTop() > 1000) {
        pagetop.fadeIn();
      } else {
        pagetop.fadeOut();
      }
    });
  });
  jQuery('.menu-trigger').on('click',function($){
    jQuery(this).toggleClass('active');
    jQuery('.g-nav').stop().slideToggle();
  });




  //ふわっと出す


    var effect_pos = 300; // 画面下からどの位置でフェードさせるか(px)
    var effect_move = 80; // どのぐらい要素を動かすか(px)
    var effect_time = 800; // エフェクトの時間(ms) 1秒なら1000

    // フェードする前のcssを定義
    $('.scroll-fade').css({
        opacity: 0,
        transform: 'translateY('+ effect_move +'px)',
        transition: effect_time + 'ms'
    });

    // ロードするたびに実行
    $(window).on('load', function(){
        $('.scroll-fade').css({
                    opacity: 1,
                    transform: 'translateY(0)'
        });
    });




  jQuery(window).on('load', function($){
    var winW = jQuery(window).width();
    if(winW <= 779){//SP

      // $('.solution-tab').slick({
      //   asNavFor: '.spec-area',
      //   dots:true
      // });
      // $('.spec-area').slick({
      //   arrows: false,
      //   swipe:false,
      // });




    }else{//PC

      jQuery('.solution-tab li').on('click',function($){
        var index = jQuery(this).index();
        jQuery('.solution-tab li').removeClass('active').eq(index).addClass('active');
        jQuery('.spec').hide();
        jQuery('.spec').eq(index).fadeIn();
      });


    }



    //コンテンツを囲む要素をidで指定

    jQuery(function($){

    if($('#gallery').length){

    var container = document.querySelector('#gallery');

      var msnry = new Masonry(container, {
        itemSelector: '.grid-item', //コンテンツのclass名
        // isFitWidth: true,
        //columnWidth: '.grid-sizer', //カラムの幅を設定
        percentPosition: true,
        horizontalOrder: false
      });

    }  else { }

    });





  });







  //
  // gradient 色変更
  //


  var colors = new Array(
    [255,160,45],
    [242,183,25],
    [242,214,25],
    [230,182,25],
    [230,112,25],
    [228,125,15]);

  var step = 0;
  //color table indices for:
  // current color left
  // next color left
  // current color right
  // next color right
  var colorIndices = [0,1,2,3];

  //transition speed
  var gradientSpeed = 0.003;

  function updateGradient()
  {

    if ( $===undefined ) return;

  var c0_0 = colors[colorIndices[0]];
  var c0_1 = colors[colorIndices[1]];
  var c1_0 = colors[colorIndices[2]];
  var c1_1 = colors[colorIndices[3]];

  var istep = 1 - step;
  var r1 = Math.round(istep * c0_0[0] + step * c0_1[0]);
  var g1 = Math.round(istep * c0_0[1] + step * c0_1[1]);
  var b1 = Math.round(istep * c0_0[2] + step * c0_1[2]);
  var color1 = "rgb("+r1+","+g1+","+b1+")";

  var r2 = Math.round(istep * c1_0[0] + step * c1_1[0]);
  var g2 = Math.round(istep * c1_0[1] + step * c1_1[1]);
  var b2 = Math.round(istep * c1_0[2] + step * c1_1[2]);
  var color2 = "rgb("+r2+","+g2+","+b2+")";

   $('#gradient').css({
    background: "-webkit-gradient(linear, left top, right top, from("+color1+"), to("+color2+"))"}).css({
    background: "-moz-linear-gradient(left, "+color1+" 0%, "+color2+" 100%)"});

    $('#gradient-l').css({
     background: "-webkit-gradient(linear, left top, right top, from("+color1+"), to("+color2+"))"}).css({
     background: "-moz-linear-gradient(left, "+color1+" 0%, "+color2+" 100%)"});


    step += gradientSpeed;
    if ( step >= 1 )
    {
      step %= 1;
      colorIndices[0] = colorIndices[1];
      colorIndices[2] = colorIndices[3];

      //pick two new target color indices
      //do not pick the same as the current one
      colorIndices[1] = ( colorIndices[1] + Math.floor( 1 + Math.random() * (colors.length - 1))) % colors.length;
      colorIndices[3] = ( colorIndices[3] + Math.floor( 1 + Math.random() * (colors.length - 1))) % colors.length;

    }
  }

  setInterval(updateGradient,10);


});
