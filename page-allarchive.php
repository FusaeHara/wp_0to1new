<?php
/**
 * The template for displaying All archives pages
 * Template Name:page-all
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package zero_to_one
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<!--pankuzuここから-->
			<?php
			$category_id = get_queried_object();
			 ?>

				<div class="pankuzu">
					<ol class="pankuzu-list">
						<li><a href="<?php echo home_url();?>">ホーム</a></li>
						<li>すべての記事</li>
					</ol>
				</div>
			<!--pankuzuここまで-->

			<h1 class="categorypage-ttl">
				<strong>すべての記事</strong>
			</h1><!-- .page-header -->

			<div class="inner-wrap">
				<div class="card-list">
						<?php
						$paged = (int) get_query_var('paged');

						$args = array(
					 'posts_per_page' => 20,
					 'paged' => $paged,
					 'orderby' => 'post_date',
					 'order' => 'DESC',
					 'post_type' => 'post',
					 'post_status' => 'publish'
					);

						$the_query = new WP_Query($args);
						if ( $the_query->have_posts() ) :
 						while ( $the_query->have_posts() ) : $the_query->the_post();

						?>

						<?php
						$cat = get_the_category();
						$cat = $cat[0];

						//親カテゴリがあるか
						if($cat->category_parent){
							$parent_cat = get_category($cat->category_parent);
							$catname = $parent_cat->name;
							$catslug = $parent_cat->slug;
						}else{
							$catname = $cat->name;//カテゴリー名
							$catslug = $cat->slug;//スラッグ名
						}
						?>

							<article class="card-article label-solution">
								<a href="<?php the_permalink(); ?>">
									<div class="card-article-thum"><?php the_post_thumbnail(); ?></div>
									<div class="<?php echo $catslug;?> card-article-label"><?php echo $catname;?></div>
									<div class="card-article-data"><?php the_time('Y-m-d')?></div>
									<div class="card-article-text"><h2><?php the_title(); ?></h2></div>
								</a>
								<?php the_tags( '<ul class="card-article-tag"><li>', '</li><li>', '</li></ul>' );?>
								</article>

							<?php endwhile;
						endif; ?>


						<!-- ページ送り -->

						<!-- ./ページ送り -->

						<?php wp_reset_postdata(); ?>

				</div><!--./cardlist -->
			</div><!--./inner-wrap-->


		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
