<?php
/**
 * The sidebar containing searchform
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package zero_to_one
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<form role="search" method="get" id="searchform" class="searchform" action="<?php echo home_url('/'); ?>">
    <div>
        <label class="screen-reader-text" for="s" style="display:none;">検索</label>
        <input type="text" value="" name="s" id="s" />
        <input type="submit" id="searchsubmit" value="検索" />
    </div>
</form>
