<?php
/**
 * The template for displaying ONLINE-EXPO posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package zero_to_one
 *
 * WP Post Template: オンライン展示会 ブース別ページ
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<!--pankuzuここから-->
				<!-- <div class="pankuzu">
					<ol class="pankuzu-list">
						<li><a href="<?php echo home_url();?>">ホーム</a></li>
						<li><a href="/online-expo/">オンライン展示会</a></li>
						<li><?php echo get_the_title();?></li>
					</ol>
				</div> -->
			<!--pankuzuここまで-->
			<div class="online-wrapper">

			<div class="bkg--single" style="background-image:url('<?php the_field('bg-slideimg'); ?>');">
				<div class="bkg--single-img">
					<!-- <img src="<?php the_field('bg-slideimg'); ?>" > -->
				</div>
			</div>

			<div class="online-single">

			<a href="/expo/" class="online-backLink"><img src="<?php echo get_stylesheet_directory_uri();?>/img/online/slider-prev-bk.png">&nbsp;オンライン展示会TOPへ</a>

			<h1 class="online-single__header">
				<div class="online-single__header__num roboto"><?php the_field('single-num')?></div>
				<div class="online-single__header__txt">
					<div>医療<span>エリア</span></div>
					<div class="online-single__header__txt--sub">ここにテキストが入りますここにテキストが入ります</div>
				</div>
			</h1>


			<div class="online-single-list">
			<?php if(have_rows('online-list')): ?>
			<?php while(have_rows('online-list')): the_row(); ?>

			<a href="" class="online-single-list__item" id="btn">
			<img src="<?php the_sub_field('online-item-mainimg'); ?>" class="ov">
			<div class="online-single-list__item__title"><?php the_sub_field('online-item-title'); ?></div>
			</a>


			<!-- modal -->
			<div id="modal" class="modal">
		  <div class="modal-content">
		    <div class="modal-body">
		      <h1>hello</h1>
		    </div>
		  </div>
			</div>
			<!-- modal -->

			<?php endwhile; ?>
			<?php endif; ?>

		</div><!-- ./online-single-list-->


		</div><!-- .online-single -->

		<nav class="nav-online">
			<div class="nav-online__inner">
			<?php

			$args = array(
					'post_type' => 'expo',
					'posts_per_page' => -1 //表示件数（-1で全ての記事を表示）
			);
			$the_query = get_posts( $args );
			if ( $the_query ) :
			foreach ( $the_query as $post ) : setup_postdata( $post );?>

			<!-- ▼loop -->
			<a class="nav-online__item" href="<?php the_permalink(); ?>">
			<div class="nav-online__item__num roboto"><?php the_field('single-num')?></div>
			<div class="nav-online__item__img"><img class="ov" src="<?php the_field('thumb-slideimg'); ?>" alt="<?php the_title(); ?>"></div>
			<p class="nav-online__item__txt"><?php the_title(); ?></p>
			</a><!-- ./swiper-slide -->
			<!-- ▲loop -->

			<?php endforeach; ?>
			<?php wp_reset_postdata(); ?>

			<?php endif; ?>

		</div>
	</nav><!-- ./nav-online__inner -->

			<div class="expo-footer">
					<div class="expo-footer__inner">
					<p><img src="<?php echo get_stylesheet_directory_uri();?>/img/common/luci3.png" width="100%"></p>
					<a href="/contact/" class="expo-footer__link">オンライン商談・お問い合わせ</a>
				 </div>
			</div>


		</div><!-- .online-wrapper -->



		</main><!-- #main -->
	</div><!-- #primary -->

</div><!-- #page -->


<?php wp_footer(); ?>
<script src="https://apis.google.com/js/platform.js" async defer>
  {lang: 'ja'}
</script>



</body>
</html>
