<?php
/**
 * The template for displaying ONLINE EXPO
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package zero_to_one
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<!--pankuzuここから-->
				<div class="pankuzu">
					<ol class="pankuzu-list">
						<li><a href="<?php echo home_url();?>">ホーム</a></li>
						<li><?php echo get_the_title(); ?></li>
					</ol>
				</div>
			<!--pankuzuここまで-->

			<div class="online-wrapper">

				<div class="online-first">

					<h1 class="online-first__title" alt="カスタム電源　オンライン展示会"><p><img src="<?php echo get_stylesheet_directory_uri();?>/img/online/img-online-title.svg"></p></h1>
					<p class="online-first__desc">こちらのサイトにて、オンライン展示会を開催中！<br />カスタム電源の強みを動画や写真にてご紹介します。</p>


					<div class="online-first__slidesec swiper-container">
						<div class="swiper-wrapper">

							<!-- ▼loop -->
							<a class="swiper-slide" href="###">
									<div class=""><img src="<?php echo get_stylesheet_directory_uri();?>/img/online/slider01_MD.png" width="100%"></div>
							</a><!-- ./swiper-slide -->
							<!-- ▲loop -->

							<!-- ▼loop -->
							<a class="swiper-slide" href="###">
									<div class=""><img src="<?php echo get_stylesheet_directory_uri();?>/img/online/slider02_MS.png" width="100%"></div>
							</a><!-- ./swiper-slide -->
							<!-- ▲loop -->


							<!-- ▼loop -->
							<a class="swiper-slide" href="###">
									<div class=""><img src="<?php echo get_stylesheet_directory_uri();?>/img/online/slider03_FA.png" width="100%"></div>
							</a><!-- ./swiper-slide -->

							<!-- ▲loop -->


							<!-- ▼loop -->
							<a class="swiper-slide" href="###">
									<div class=""><img src="<?php echo get_stylesheet_directory_uri();?>/img/online/slider04_MC.png" width="100%"></div>
							</a><!-- ./swiper-slide -->
							<!-- ▲loop -->

							<!-- ▼loop -->
							<a class="swiper-slide" href="###">
									<div class=""><img src="<?php echo get_stylesheet_directory_uri();?>/img/online/slider05_SP.png" width="100%"></div>
							</a><!-- ./swiper-slide -->


							<!-- ▲loop -->


						</div><!-- ./swiper-wrapper -->
					</div><!-- ./swiper-container -->


					<div class="online-nav">
						<div class="swiper-button-prev"><img src="<?php echo get_stylesheet_directory_uri();?>/img/online/slider-prev.png" width="100%"></div>

						<div class="swiper-button-next"><img src="<?php echo get_stylesheet_directory_uri();?>/img/online/slider-next.png" width="100%"></div>

					</div>


				</div><!-- ./online-first -->

				</div><!-- ./online-wrapper -->










		</main><!-- #main -->
	</div><!-- #primary -->


<?php
get_footer();
