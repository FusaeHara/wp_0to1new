<?php
/**
 * The template for displaying front pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package zero_to_one
 */

get_header(); ?>

<div class="front--grad" id="gradient-l">
  <div class="front--grad__inner">
  <?php get_template_part( 'template-parts/stick','banner' ); ?>
  </div>
</div>



  <section class="sec-whatsnew sec">
    <div class="sec-whatsnew__inner flexbox">
    <!--What's new-->
      <div class="sec-whatsnew__col">
      <h2 class="sec-whatsnew__ttl">What’s NEW</h2>
      		<ul class="whatsnew-list">
            <?php
            $args = array( 'posts_per_page' => 7, 'category_name' => 'whatsnew', 'orderby'=> 'date','order'=> 'DESC' );
            $wnposts = get_posts( $args );
            foreach ( $wnposts as $post ) : setup_postdata( $post ); ?>
            <li>
            <?php  $icon = get_field('icon-wn'); ?><dl><dt><?php the_time('Y-m-d');?></dt><dd class="whatsnew-list__ddicon"><img src="<?php echo get_stylesheet_directory_uri();?>/img/top/icon-<?php echo $icon; ?>.png" /></dd><dd class="whatsnew-list__ddlink"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></dd></dl>
            </li>
            <?php endforeach; wp_reset_postdata();?>
          </ul>

          <a href="<?php echo esc_url( home_url( '/' ) ); ?>category/whatsnew/" class="btn btn-more whatsnew-more">一覧</a>

        </div>
      <!-- ./What'snew -->

      <div class="flexbox">
      <!-- seasonal banner -->
      <?php get_template_part( 'template-parts/seasonal','banner' ); ?>
      <!--./ seasonal banner -->


      <!-- cv banner -->
      <?php get_template_part( 'template-parts/cv','banner' ); ?>
      <!--./ cv banner -->
    </div>

    </div>
  </section>



<?php
  get_footer();
