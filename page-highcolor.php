<?php
/**
 * The template for displaying 検査
 * Template Name:page-highcolor
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package zero_to_one
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<!--pankuzuここから-->
			<?php
			$category_id = get_queried_object();
			 ?>

				<div class="pankuzu">
					<ol class="pankuzu-list">
						<li><a href="<?php echo home_url();?>">ホーム</a></li>
						<li>検査</li>
					</ol>
				</div>
			<!--pankuzuここまで-->

			<div>
			<h1 class="header--grad header--grad--high" id="gradient">
				<span class="header--grad__ttl header--grad--high__ttl">検査</span>
			</h1>

				<!-- == PRODUCTS === -->

				<div class="lay-high__prolist flexbox">

					<?php
					$args = array( 'posts_per_page' => 5, 'category_name' => 'p-highcolor', 'orderby'=> 'menu_order','order'=> 'DESC' );
					$wnposts = get_posts( $args );
					foreach ( $wnposts as $post ) : setup_postdata( $post ); ?>

					<a href="<?php the_permalink(); ?>" class="lay-high__prolist__item">
						<div class="card-article-thum card-article-thum--high"><?php the_post_thumbnail(); ?></div>
						<div class="card-article-text"><h2><?php the_title(); ?></h2></div>
					</a>

					<?php endforeach; wp_reset_postdata();?>

				</div>

				<!-- == //PRODUCTS === -->
			</div><!-- .page-header -->

			<div class="lay-high">

				<!--　▼ content area ▼ -->
				<div class="lay-high__inner flexbox">

				<!-- == WORKS === -->
				<div class="lay-high__inner--works">
					<h2 class="lay-high__sub-column__h">
					Works <span>納入事例</span>
					</h2>


						<div class="lay-high__card-article flexbox">
							<?php
							$args = array( 'posts_per_page' => -1, 'category_name' => 's-highcolor', 'orderby'=> 'menu_order_num','order'=> 'DESC' );
							$wnposts = get_posts( $args );
							foreach ( $wnposts as $post ) : setup_postdata( $post ); ?>


							<a href="<?php the_permalink(); ?>" class="lay-high__card-article__item">
								<div class="lay-high__card-article-thum"><?php the_post_thumbnail(); ?></div>
								<div class="lay-high__card-article-text"><?php the_title(); ?></div>
							</a>

							<?php endforeach; wp_reset_postdata();?>
						</div>
				</div>
					<!-- == //WORKS === -->


					<!-- == download === -->
					<div class="lay-high__inner--downloads">
						<h2 class="lay-high__sub-column__h lay-high__sub-column__h--d">Downloads　<span>資料ダウンロード</span></h2>

							<ul class="lay-high__download">

								<?php  query_posts( array(
								'post_type'=>'downloads',
								'meta_key'=>'pdf-cate-1',/*カスタムフィールド（検査）*/
								'meta_value'=>true,/*カスタムフィールドの値*/
								) );
								?>

								<?php if (have_posts()) : while (have_posts()) : the_post();
								?>
								<li class="lay-high__download__item">
									<a href="<?php the_field('pdf');?>" target="_blank">
										<div class="lay-high__download__img"><img src="<?php the_field('cover-img')?>" alt="<?php the_field('title');?>"></div>
										<div class="lay-high__download__txt">
											<p class="lay-high__download__txt__title"><?php the_field('title');?></p>
											<p class="lay-high__download__txt__desc"><?php the_field('filename');?><br /><?php the_field('datasize');?></p>
										</div>
									</a>
								</li>

								<?php endwhile; endif; ?>

							</ul>

						<!-- == //download === -->

				</div>

				<!--　▲ content area ▲ -->



			</div><!--./inner-wrap-->


		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
