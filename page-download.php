<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package zero_to_one
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<!--pankuzuここから-->
				<div class="pankuzu">
					<ol class="pankuzu-list">
						<li><a href="<?php echo home_url();?>">ホーム</a></li>
						<li><?php echo get_the_title(); ?></li>
					</ol>
				</div>
			<!--pankuzuここまで-->

			<h1 class="categorypage-ttl"><strong>Download</strong>
			<span>資料・カタログのダウンロード</span></h1>

			<div class="inner-wrap dis-flex dis-flex-3col">

						<?php  query_posts( array(
	    			'post_type'=>'downloads',
						'post_status' => 'publish',
	        	'paged' => get_query_var('paged')
						) );
						?>

						<?php if (have_posts()) : while (have_posts()) : the_post();
						?>

						<section class="download-col three-col">
						 	<a href="<?php the_field('pdf');?>" target="_blank">
								<h3 class="download-ttl"><?php the_field('title');?></h3>
								<div class="download-col-inner">
									<div class="download-thum">
									<img src="<?php the_field('cover-img')?>" alt="<?php the_field('title');?>">
									</div>
									<p class="download-text">><?php the_field('filename');?><br /><?php the_field('datasize');?></p>
								</div>
							</a>
						</section>

						<?php endwhile; endif; ?>


					</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
