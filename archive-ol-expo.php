<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package zero_to_one
 */

get_header(); ?>

	<div id="primary" class="content-area content-area-expo">

	<main id="main" class="site-main" role="main">

		<!--pankuzuここから-->
			<!-- <div class="pankuzu">
				<ol class="pankuzu-list">
					<li><a href="<?php echo home_url();?>">ホーム</a></li>
					<li>カスタム電源 オンライン展示会</li>
				</ol>
			</div> -->
		<!--pankuzuここまで-->

		<div class="online-wrapper">

			<div class="bkg" style="background-image:url('<?php echo get_stylesheet_directory_uri();?>/img/online/img-bg-online01.jpg');">
				<!-- <div class="bkg-img">
					<img src="<?php echo get_stylesheet_directory_uri();?>/img/online/img-bg-online01.jpg" />
				</div> -->
			</div>

			<div class="online-first scroll-fade">
				<h1 class="online-first__title" alt="カスタム電源　オンライン展示会"><p><img src="<?php echo get_stylesheet_directory_uri();?>/img/online/img-online-title.svg"></p></h1>
				<p class="online-first__desc">こちらのサイトにて、オンライン展示会を開催中！<br />カスタム電源の強みを動画や写真にてご紹介します。</p>


				<div class="swiper-div">

					<div class="online-first__slidesec swiper-container">
						<div class="swiper-wrapper">



							<?php
							$args = array(
							    'orderby'       => 'name',
							    'order'         => 'ASC',
							);
							$taxonomy_name = 'expo_cat'; //表示したいtaxonomynameを設定、ここではターム名をitem_categoryにしたのでそれを入れてます。
							$taxonomys = get_terms($taxonomy_name , $args);
							if(!is_wp_error($taxonomys) && count($taxonomys)):
							    foreach($taxonomys as $taxonomy):
							        $term_id = esc_html($taxonomy->term_id);
							        $term_idsp = "expo_cat_".$term_id; //カスタムフィールドを取得するのに必要なtermのIDは「taxonomyname_ + termID」
							        $photo = get_field('first-slideimg',$term_idsp);
							        $photosp = wp_get_attachment_image_src($photo, 'full');
							?>

							<!-- ▼loop -->
							<a class="swiper-slide" href="/ol-expo/category/<?php echo esc_html($taxonomy->slug); ?>/">
								<img class="ov" src="<?php echo $photosp[0]; ?>" alt="<?php echo esc_html($taxonomy->name); ?>">
							</a>
							<!-- ▲loop -->

								<?php
								    endforeach;
								endif;
								?>

						</div><!-- ./swiper-wrapper -->

					</div><!-- ./swiper-container -->

				<!-- nav -->
				<div class="online-nav">
						<div class="swiper-button-prev"><!-- --></div>
						<div class="swiper-button-next"><!-- --></div>
				</div>

			</div>


			</div><!-- ./online-first -->


			<div class="expo-footer">
					<div class="expo-footer__inner">
					<p><img src="<?php echo get_stylesheet_directory_uri();?>/img/common/luci3.png" width="100%"></p>
					<a href="/contact/" class="expo-footer__link">オンライン商談・お問い合わせ</a>
				 </div>
			</div>


		</div><!-- ./online-wrapper -->






	</main><!-- #main -->

	</div><!-- #primary -->


</div><!-- #page -->


<?php wp_footer(); ?>
<script src="https://apis.google.com/js/platform.js" async defer>
  {lang: 'ja'}
</script>


<script>


  var mySwiper = new Swiper ('.swiper-container', {
    direction: 'horizontal',
    effect: 'slide',
		loop: true,
		loopAdditionalSlides: 8,
		slidesPerView: 2,
		centeredSlides: false,
		resistanceRatio: 1.5,
    spaceBetween: 0,
		touchRatio: 1,
		freeModeSticky: true,
		touchAngle: 45,
		followFinger: false,
		//grabCursor: true,
		autoplay: {
	    delay: 5000,
	    disableOnInteraction: true,
	  },
    mousewheel: {
      invert: true,
    },
		navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
		},

		breakpoints: {

			640: {
				loop: true,
				//loopedSlides: 5,
				slidesPerView: 2,
				centeredSlides: true,
				touchRatio: 0.8,
				speed: 300,
			},
			960: {
				loop: true,
				//loopedSlides: 5,
				touchRatio: 3,
				slidesPerView: 5,
				centeredSlides: false,
				speed: 1000,
				freeMode: true,
				longSwipes: true,
				longSwipesMs:20,
			}

		},

  });

	var mySwiper = document.querySelector('.swiper-container').swiper


</script>



</body>
</html>
