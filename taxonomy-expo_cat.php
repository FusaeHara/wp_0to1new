<?php
/**
 * The template for displaying category at online-expo
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package zero_to_one
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">


			<?php
				$term_id = get_queried_object_id();  //カテゴリIDの取得

				 $num = get_field('single-num','expo_cat_'.$term_id); //ナンバー
				 $txt = get_field('cat-subtxt','expo_cat_'.$term_id);	//サブキャッチ
				 $imgurl_t = get_field('thumb-slideimg','expo_cat_'.$term_id);
 			 	 $imgurl_b = get_field('bg-slideimg','expo_cat_'.$term_id);
			?>

			<div class="online-wrapper">

			<div class="bkg--single" style="background-image:url('<?php echo $imgurl_b; ?>');">
				<!-- background -->
			</div>

			<div class="online-single">

			<a href="/ol-expo/" class="online-backLink"><img src="<?php echo get_stylesheet_directory_uri();?>/img/online/slider-prev-bk.png">&nbsp;オンライン展示会TOPへ</a>

			<h1 class="online-single__header">
				<div class="online-single__header__num roboto">
					<?php echo $num; ?>
				</div>
				<div class="online-single__header__txt">
					<div class="online-single__header__txt--main"><?php echo the_archive_title(); ?><span>エリア</span></div>
					<div class="online-single__header__txt--sub"><?php echo $txt;?></div>
				</div>
			</h1>


			<div class="online-single-list">

				<?php if (have_posts() ) :  while(have_posts()):the_post();?>

					<!-- モーダルフラグバナー -->
					<a data-remodal-target="modal-<?php the_ID();?>" class="online-single-list__item scroll-fade">
						<img src="<?php the_field('online-item-mainimg'); ?>">
						<div class="online-single-list__item__title"><?php echo the_title(); ?></div>
					</a>

					<!-- モーダルコンテンツ -->

								<div class="remodal online-detail-wrap" data-remodal-id="modal-<?php the_ID();?>">

									<button data-remodal-action="close" class="remodal-close"><!-- --></button>

									<div class="online-detail-inner">

										<h2 class="online-detail__title"><?php the_field('online-item-title'); ?></h2>

										<div class="online-detail__intro">
											<div class="online-detail__pimg__wrap">
												<a href="<?php the_field('online-item-pfile'); ?>" class="online-detail__pimg" target="_blank">
													<img src="<?php the_field('online-item-pimg'); ?>">
												</a>
											</div>
											<div class="online-detail__hbox">
												<p class="online-detail__h"><?php the_field('online-item-h'); ?></p>
												<p class="online-detail__txt"><?php the_field('online-item-txt'); ?></p>
												<p class="online-detail__subh">特徴</p>
												<p class="online-detail__txt"><?php the_field('online-item-spec'); ?></p>

													<div class="online-detail__pimg__link__wrap">
													<a href="<?php the_field('online-item-pfile'); ?>" class="online-detail__pimg__link" target="_blank">
														<div class="icon"><img src="<?php echo get_stylesheet_directory_uri();?>/img/common/icon_download-w.png"></div><span class="txt">PDFをダウンロード</span></a>
													</div>

											</div>
										</div>

										<?php
											$edit = get_field('online-item-edit');
											if($edit){ ?>
												<div class="online-detail__second">
													<?php echo $edit; ?>
												</div>
										<? } ?>

										<?php
											$mov = get_field('online-item-iframe');
											if($mov){ ?>

											<div class="online-detail__movie embed-container">
												<?php echo $mov; ?>
											</div>
										<? } ?>

										<!-- クローズ -->

									</div><!-- ./online-detail-inner -->


									<button data-remodal-action="close" class="online-detail__closebnr">このウィンドウを閉じる</button>



								</div><!-- ./online-detail-wrap -->

					<!-- ./ モーダルコンテンツ -->


				<!--▲ loop -->


				<?php endwhile; endif;
				wp_reset_postdata(); ?>


				</div><!-- ./online-single-list-->



			</div><!-- .online-single -->

			<nav class="nav-online">
				<div class="nav-online__inner">

				<?php
					$taxonomy_name = 'expo_cat'; //表示したいtaxonomynameを設定、ここではターム名をitem_categoryにしたのでそれを入れてます。

					$args = array(
						'orderby'       => 'name',
    				'order'         => 'ASC',
					);

					$taxonomys = get_terms($taxonomy_name,$args);
					if(!is_wp_error($taxonomys) && count($taxonomys)):
					 foreach($taxonomys as $taxonomy):
				?>

				<!-- loop -->
				<?php
					$term_id = esc_html($taxonomy->term_id);
					$num = get_field('single-num','expo_cat_'.$term_id);
					$thumb = get_field('thumb-slideimg','expo_cat_'.$term_id);
				?>

				<a class="nav-online__item" href="/ol-expo/category/<?php echo esc_html($taxonomy->slug); ?>/">
						<div class="nav-online__item__num roboto"><?php echo $num; ?></div>
						<div class="nav-online__item__img"><img class="ov" src="<?php echo $thumb; ?>" alt="<?php echo esc_html($taxonomy->name); ?>"></div>
						<p class="nav-online__item__txt"><?php echo esc_html($taxonomy->name); ?></p>
				</a>

				<?php
					endforeach;
					endif;
				?>

				</div>
			</nav><!-- ./nav-online__inner -->


			<div class="expo-footer">
					<div class="expo-footer__inner">
					<p><img src="<?php echo get_stylesheet_directory_uri();?>/img/common/luci3.png" width="100%"></p>
					<a href="/contact/" class="expo-footer__link">オンライン商談・お問い合わせ</a>
				 </div>
			</div>


	</div><!-- .online-wrapper -->



	</main><!-- #main -->
	</div><!-- #primary -->

	</div><!-- #page -->


	<?php wp_footer(); ?>
	<script src="https://apis.google.com/js/platform.js" async defer>
	{lang: 'ja'}
	</script>
