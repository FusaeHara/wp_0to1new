<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package zero_to_one
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
<link rel="profile" href="http://gmpg.org/xfn/11">

<link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:ital@1&display=swap" rel="stylesheet">

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5QXGWTS');</script>
<!-- End Google Tag Manager -->
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QXGWTS"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) —>

	<!--ga-->
	<?php include_once("analyticstracking.php") ?>
	<!-- ./ga-->

	<!--facebook SDK -->
	<div id="fb-root"></div>
		<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.9";
		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
	<!--#facebook SDK-->

<div id="page" class="site">

	<header id="masthead" class="header" role="banner">
		<div class="header-inner">
			<?php
			if ( is_front_page() && is_home() ) : ?>
				<h1 class="sitelogo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo get_stylesheet_directory_uri();?>/img/common/sitelogo.svg" alt="zero to one project 0から1を生み出す"></a></h1>
			<?php else : ?>
				<p class="sitelogo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo get_stylesheet_directory_uri();?>/img/common/sitelogo.svg" alt="zero to one project 0から1を生み出す"></a></p>
			<?php
			endif;?>

			<?php wp_nav_menu( array( 'menu_id' => 'primary-menu','menu_class' => 'g-nav','items_wrap'=> '<ul id="%1$s" class="%2$s" aria-controls="primary-menu">%3$s</ul>' ) ); ?>

			<div class="luci"><a href="https://www.luci.co.jp/jp/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri();?>/img/common/luci.svg" alt=""></a></div>

		</div>
	</header><!-- #masthead -->
	<div class="h-token"></div>
	<a class="menu-trigger" href="javascript:void(0)">
		<span></span>
		<span></span>
		<span></span>
	</a>
<!--headerここまで-->

	<div id="content" class="site-content">
